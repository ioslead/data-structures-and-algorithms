//Match.test()
import Foundation
/*
let arr = [2,5,3,7,9,11,25,17]
kthSmallest(arr, k: 5)
kthLargest(arr, k: 5)
kLargestElements(arr, k: 5)

var arr_k_sorted = [6,5,3,2,8,10,9]
arr_k_sorted.sortByK(3)

arr_k_sorted.kClosestElements(x: 2, k: 4)
var arr_freq = [1,1,1,4,2,2,3]
arr_freq.kFrequent(2)
arr_freq.sortByFrequency()

let origin = CGPoint(x:0,y:0)
let p1 = CGPoint(x: 2, y: 2)
let p2 = CGPoint(x: 2, y: 1)
let p3 = CGPoint(x: 1, y: 1)
let p4 = CGPoint(x: 1.2, y: 1.5)

let arr_points = [p1, p2, p3, p4]
arr_points.kClosestPointTo(origin, k: 2)

[1,2,3,4,5].minCostToConnectRopes()

arr_k_sorted.sumOfElements(1, k2: 4)

[1,3,6,4].nsl()
[1,3,6,4].nsl1()

[1,3,6,4].nsr()
[1,3,6,4].ngl()
[1,3,6,4].ngr()
[100, 80, 60, 70, 60, 75, 85].stockSpan()
[7,5,7,10,2,3].mah()

let mat_bin:[[Int]] = [[0,1,1,0],[1,1,1,1],[1,1,1,1,],[1,1,0,0]]
maxAreaRectInBinaryMatrix(mat: mat_bin)

var min_stack = EfficientMinStack()//MinStack()
min_stack.push(18)
min_stack.push(19)
min_stack.push(29)
min_stack.push(15)
min_stack.push(16)
min_stack.getMin()
min_stack.pop()
min_stack.pop()
min_stack.pop()
min_stack.pop()
min_stack.getMin()

LinkedList.test()
print("ok")
TNode.test()

var heap = Heap<Int>{
    $0 < $1 //min hap
}
[100, 80, 60, 70, 60, 75, 85].forEach { (e) in
    heap.insert(e)
}
print(heap)
heap.remove()
heap.remove()
heap.remove()

print(heap)
Match.test()

var qs = QueueWithStack<Int>()
qs.enQueue(2)
qs.enQueue(4)
qs.enQueue(1)
qs.enQueue(6)
qs.enQueue(7)
qs.enQueue(0)
qs.deQueue()
qs.deQueue()
qs.enQueue(12)
qs.deQueue()

var l1 = ListNode(value: 9)
l1.next = ListNode(value: 9)
l1.next?.next = ListNode(value: 9)
l1.next?.next?.next = ListNode(value: 9)
l1.next?.next?.next?.next = ListNode(value: 9)
l1.next?.next?.next?.next?.next = ListNode(value: 9)
l1.next?.next?.next?.next?.next?.next = ListNode(value: 9)


var l2 = ListNode(value: 9)
l2.next = ListNode(value: 9)
l2.next?.next = ListNode(value: 9)
l2.next?.next?.next = ListNode(value: 9)

let result = addTwoNumbers(l1, l2)
print("ans =")
result?.printList()
 
//BSTree.test()

let node0 = BNode<Int>(0)
let node1 = BNode<Int>(1)
let node2 = BNode<Int>(2)
let node3 = BNode<Int>(3)
let node4 = BNode<Int>(4)
let node5 = BNode<Int>(5)
let node6 = BNode<Int>(6)
let node7 = BNode<Int>(7)

node0.left = node1
node0.right = node2
node1.left = node3
node1.right = node4
node2.left = node5
node4.left = node6
node4.right = node7

printPaths(node0, nil)
convertToMirrorTree(node0)
print("Mirror tree")
printPaths(node0, nil)

let node00 = BNode<Int>(10)
let node01 = BNode<Int>(3)
let node02 = BNode<Int>(-7)
let node03 = BNode<Int>(5)
let node04 = BNode<Int>(4)

let node012 = BNode<Int>(12)
let node08 = BNode<Int>(10)
let node015 = BNode<Int>(15)
let node017 = BNode<Int>(17)

node00.left = node01
node00.right = node02
node02.left = node03
node02.right = node04
node01.right = node012
node012.right = node08
node08.right = node015
node015.right = node017
*/

//hasChildSumProperty(node00)
//node00.maxSumLeafToLeaf()
//node00.maxSumNodeToNode()
//node00.findSumPath(8)
//checkSumPath(node00, 8)
//node00.verticalSum()
//node00.printAllLeaves()
//node00.printLeftView()

//node00.printLeftViewRec()
//node00.printTopView()
//node00.printRightViewRec()
/*
var bst = BSTree<Int>()
bst.insert(2)
bst.insert(12)
bst.insert(22)
bst.insert(4)
bst.insert(1)
bst.inorderSuccessorBSTNode(BSTNode(2))?.value
bst.kthSmallestInBST(4)
bst.printAncestors(22)
 
var pre_index = 0
let root = createBTFrom([25, 50, 75, 100, 125, 150, 200], [100, 50, 25, 75, 150, 125,200], &pre_index)
root?.inorder()
*/

//let root_tree = createTreeFrom([2,1,3],[1,2,3])
let s = test_Paran()
print(s)
