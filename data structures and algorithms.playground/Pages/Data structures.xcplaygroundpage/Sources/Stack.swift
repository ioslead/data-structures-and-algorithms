import Foundation

public struct Stack<T> {
    private var array:[T] = []
    public mutating func push(_ value: T){
        array.append(value)
    }
    @discardableResult
    public mutating func pop()->T? {
        array.isEmpty ? nil : array.popLast()
    }
    public var isEmpty:Bool {
        return array.count == 0
    }
    public var top:T?{
        array.last
    }
    public init(){}
}
