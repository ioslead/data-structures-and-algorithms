import Foundation

public class TriNode<Key: Hashable>{
    public var key:Key?
    public var children:[Key: TriNode] = [:]
    public weak var parent:TriNode?
    public var isTerminating = false
    public init(key: Key?, parent: TriNode?){
        self.key = key
        self.parent = parent
    }
}

public struct Tri<CollectionType: Collection> where CollectionType.Element : Hashable{
    private var root = TriNode<CollectionType.Element>(key: nil, parent: nil)
    public init(){}
}

public extension Tri {
    func insert(_ collection: CollectionType){
        var current = root
        for element in collection {
            if current.children[element] == nil {
                current.children[element] = TriNode<CollectionType.Element>(key: element, parent: current)
            }
            current = current.children[element]!
        }
        current.isTerminating = true
    }
    
    @discardableResult
    func contains(_ collection: CollectionType)->Bool{
        var current = root
        for element in collection {
            guard  let child = current.children[element] else {
                return false
            }
            current = child
        }
        return current.isTerminating
    }
    func remove(_ collection: CollectionType){
        var current = root
        for element in collection {
            guard let child = current.children[element] else {
                return
            }
            current = child
        }
        guard current.isTerminating else {
            //non terminating node
            return
        }
        current.isTerminating = false
        //leaf node
        while let parent = current.parent, current.children.isEmpty && !current.isTerminating {
            parent.children[current.key!] = nil
            current = parent
        }
    }
}
public extension Tri where CollectionType : RangeReplaceableCollection{
    func collection(startWith prefix:CollectionType) -> [CollectionType] {
        var current = root
        for element in prefix {
            guard  let child = current.children[element] else {
                return []
            }
            current = child
        }
        /// prefix found, current is the node represented by the last element of the prefix
        return collection1(startWith: prefix, after: current)
    }
    private func collection1(startWith prefix: CollectionType, after node: TriNode<CollectionType.Element>)->[CollectionType]{
        var results:[CollectionType] = []
        if node.isTerminating { /// prefix is a valid string
            results.append(prefix)
        }
        for child in node.children.values {
            var updatedPrefix = prefix
            updatedPrefix.append(child.key!)
            results.append(contentsOf: collection1(startWith: updatedPrefix, after: child))
        }
        return results
    }
}

//MARK:- Test
public extension Tri where CollectionType == String{
    static func test(){
        let tri = Tri<String>()
        tri.insert("cute")
        tri.insert("cut")
        tri.insert("pankaj")
        tri.insert("verma")
        tri.insert("pavan")
        tri.insert("pawan")
        tri.insert("ppl")
        tri.insert("cheetah")
        tri.insert("cheat")
        
        tri.contains("cut")
        
        let results = tri.collection(startWith: "chea")
        print(results)
    }
}
