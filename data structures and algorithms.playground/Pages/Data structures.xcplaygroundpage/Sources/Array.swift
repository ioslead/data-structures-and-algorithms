import Foundation

public func arr1<T>(row: Int, col: Int, initialVal:T) -> [[T]] {
    return Array(repeating: Array(repeating: initialVal, count: row), count: col)
}

