import Foundation
public class TNode <T> {
    public var value: T
    public var children: [TNode] = []
    public init(_ value: T){
        self.value = value
    }
}
extension TNode {
    public func addChild(_ node: TNode){
        children.append(node)
    }
    /// dfs traversal
    public func dfs(prefix:String = "", visit: (TNode, String)->Void){
        visit(self, prefix)
        let newPrefix = prefix + "|--"
        children.forEach{
            $0.dfs(prefix: newPrefix, visit: visit)
        }
    }
    
    /// level order traversal
    public func lot(visit: (TNode)->Void){
        visit(self)
        var Q = QueueArr<TNode>()
        children.forEach{
            Q.enQueue($0)
        }
        while let node = Q.deQueue() {
            visit(node)
            node.children.forEach{
                Q.enQueue($0)
            }
        }
    }
    
    /// level order traversal
    public func levelOrderTraversal(visit: (TNode)->Void){
        var Q = QueueArr<TNode>()
        Q.enQueue(self)
        while !Q.isEmpty {
            if let n = Q.deQueue() {
                visit(n)
                n.children.forEach {
                    Q.enQueue($0)
                }
            }
        }
    }
}

extension TNode where T: Equatable {
    /// search
    public func search(_ value: T)->TNode?{
        var result:TNode?
        lot {
            if $0.value == value {result = $0}
        }
        return result
    }
}

//// ***************************queue*****************************

public protocol QueueProtocol {
    associatedtype Element
    mutating func enQueue(_ element: Element)
    mutating func deQueue()->Element?
    var peek:Element?{get}
    var isEmpty:Bool {get}
}

public struct QueueArr<T> : QueueProtocol {
    /// time complexity: O(1)
    public mutating func enQueue(_ element: T) {
        array.append(element)
    }

    /// time complexity: O(n)
    @discardableResult
    public mutating func deQueue() -> T? {
        isEmpty ? nil : array.removeFirst()
    }

    public var peek: T? {
        array.first
    }

    public var isEmpty: Bool {
        array.isEmpty
    }

    public typealias Element = T

    private var array:[T] = []
//    public init(){}

}
extension QueueArr: CustomStringConvertible {
    public var description: String {
        return array.description
    }
}

//MARK:- Test
public extension TNode where T == String{
    static func test(){
        
        let root = TNode("root")
        let rc1 = TNode("rc1")
        let rc2 = TNode("rc2")
        let rc3 = TNode("rc3")
        let rc11 = TNode("rc11")
        
        root.addChild(rc1)
        root.addChild(rc2)
        root.addChild(rc3)
        rc1.addChild(rc11)
        
        root.dfs { node, prefix  in
            let postfix = prefix.isEmpty ? "" : "-->"
            print(prefix+"\(node.value)"+postfix)
        }
        
        root.lot { node  in
            print("\(node.value)")
        }
        print("____________")
        root.levelOrderTraversal{ node  in
            print("\(node.value)")
        }
        
        let resultNode = root.search("rc4")
        print(resultNode?.value ?? "no node found!\n\n")
        
    }
}
