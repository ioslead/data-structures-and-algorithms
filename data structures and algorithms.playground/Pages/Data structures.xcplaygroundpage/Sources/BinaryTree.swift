import Foundation

public class BNode<T> {
    public var value: T
    public var left: BNode?
    public var right: BNode?
    public init(_ value: T){
        self.value = value
    }
}

extension BNode {
    public func inorder(){
        inorder(self)
    }
    private func inorder(_ node: BNode?){
        guard let node = node else {
            return
        }
        var s = Stack<BNode>()
        s.push(node)
        var temp:BNode? = node
        while !s.isEmpty {
            while temp != nil {
                s.push(temp!)
                temp = temp?.left
            }
            temp = s.pop()
            if let value = temp?.value {
                print(value)
            }
            temp = temp?.right
        }
    }
    public func inorder1(){
        var s = Stack<BNode>()
        s.push(self)
        
    }
}

extension BNode {
    private func diagram(for node:BNode?, top: String = "", root: String = "", bottom: String = "")-> String{
        guard let node = node else {
            return root + "nil\n"
        }
        if node.left == nil && node.right == nil {
            return root + "\(node.value)\n"
        }
        let l =  diagram(for: node.right, top: top+"   ", root: top+"┌──", bottom: top+"│ ")
        let m = root+"\(node.value)\n"
        let r = diagram(for: node.left, top: bottom+"│ ", root: bottom+"└──", bottom: bottom+"   ")
        return l+m+r
    }
    public var description: String {
        return diagram(for: self)
    }
}


//MARK:- Test
public extension BNode where T == String {
    static func test(){
        let a = BNode("A")
        let b = BNode("B")
        let c = BNode("C")
        let d = BNode("D")
        let e = BNode("E")
        let f = BNode("F")
        let g = BNode("G")
        
        a.left = b
        a.right = c
        b.left = d
        b.right = e
        c.left = f
        c.right = g
        
        print(a.description)
        
        a.inorder()
    }
}
