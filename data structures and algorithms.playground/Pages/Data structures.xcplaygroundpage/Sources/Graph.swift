import Foundation

public enum EdgeType{
    case directed
    case undirected
}
public struct Vertex<T> {
    public let index: Int
    public let data: T
}
public struct Edge<T> {
    public let source: Vertex<T>
    public let destination: Vertex<T>
    public let weight: Double?
}

public protocol Graph{
    associatedtype Element
    func createVertex(data:Element)->Vertex<Element>
    func add(_ edge:EdgeType, from source: Vertex<Element>, to destination: Vertex<Element>, weight: Double?)
    func addUndirectedEdge(from source: Vertex<Element>, to destination: Vertex<Element>, weight: Double?)
    func addDirectedEdge(from source: Vertex<Element>, to destination: Vertex<Element>, weight: Double?)
    func edges(from source:Vertex<Element>)->[Edge<Element>]
    func weight(from source:Vertex<Element>, to destination:Vertex<Element>)->Double?
}

///default implementations
extension Graph {
    public func add(_ edge:EdgeType, from source: Vertex<Element>, to destination: Vertex<Element>, weight: Double?){}
    public func addUndirectedEdge(from source: Vertex<Element>, to destination: Vertex<Element>, weight: Double?){}
}

///to use vertex as key make is Hashable
extension Vertex:Hashable {
    public func hash(into hasher: inout Hasher) {
        hasher.combine(index)
    }
    public static func == (lhs: Vertex<T>, rhs: Vertex<T>) -> Bool {
        lhs.index == rhs.index
    }
}

//MARK:- AdjacencyList
public class AdjacencyList<T>: Graph {
    public typealias Element = T
    private var adjacencies: [Vertex<T>: [Edge<T>]] = [:]
    public init() {}
    
    public func createVertex(data: T) -> Vertex<T> {
        let vertex = Vertex(index: adjacencies.count, data: data)
        adjacencies[vertex] = []
        return vertex
    }
    
    public func add(_ edge: EdgeType, from source: Vertex<T>, to destination: Vertex<T>, weight: Double?) {
        switch edge {
        case .directed: break
            
        case .undirected: break
            
        }
    }
    public func addUndirectedEdge(from source: Vertex<Element>, to destination: Vertex<Element>, weight: Double?){
        addDirectedEdge(from: source, to: destination, weight: weight)
        addDirectedEdge(from: destination, to: source, weight: weight)
    }
    public func addDirectedEdge(from source: Vertex<Element>, to destination: Vertex<Element>, weight: Double?){
        let edge = Edge(source: source, destination: destination, weight: weight)
        adjacencies[source]?.append(edge)
    }
    public func edges(from source: Vertex<T>) -> [Edge<T>] {
        return adjacencies[source] ?? []
    }
    
    public func weight(from source: Vertex<T>, to destination: Vertex<T>) -> Double? {
        return edges(from: source).first { $0.destination == destination }? .weight
    }
    
}

//MARK:- AdjacencyMatrix
public class AdjacencyMatrix<T>: Graph {
    
    private var vertices: [Vertex<T>] = []
    private var weights: [[Double?]] = []
    public init() {}
    
    public func createVertex(data: T) -> Vertex<T> {
        let vertex = Vertex(index: vertices.count, data: data)
        vertices.append(vertex)
        for i in 0..<weights.count {
            weights[i].append(nil)
        }
        let row = [Double?](repeating: nil, count: vertices.count)
        weights.append(row)
        return vertex
    }
    public func addDirectedEdge(from source: Vertex<T>, to destination: Vertex<T>, weight:Double?){
        weights[source.index][destination.index] = weight
    }
    public func edges(from source: Vertex<T>) -> [Edge<T>] {
        var edges: [Edge<T>] = []
        for column in 0..<weights.count {
            if let weight = weights[source.index][column] {
                edges.append(Edge(source: source, destination: vertices[column], weight: weight))
            }
        }
        return edges
    }
    public func weight(from source: Vertex<T>, to destination: Vertex<T>) -> Double? {
        return weights[source.index][destination.index]
    }
    
}

extension Graph {
    //MARK:- BFS
    func breadthFirstSearch(from source: Vertex<Element>) -> [Vertex<Element>] {
        var queue = QueueStack<Vertex<Element>>()
        var enqueued: Set<Vertex<Element>> = []
        var visited: [Vertex<Element>] = []
        queue.enqueue(source) // starting node
        enqueued.insert(source)
        while let vertex = queue.dequeue() { // dequeue until the queue is empty.
            visited.append(vertex) // add vertex to the list of visited vertices.
            let neighborEdges = edges(from: vertex) // find all edges that start from the current vertex and iterate over them.
            neighborEdges.forEach { edge in
                if !enqueued.contains(edge.destination) { // check if destination vertex has been enqueued before, and if not, add it to the code.
                    queue.enqueue(edge.destination)
                    enqueued.insert(edge.destination)
                }
            }
        }
        return visited
    }
    //MARK:- DFS
    func depthFirstSearch(from source: Vertex<Element>)
    -> [Vertex<Element>] {
        var stack =  StackArr<Vertex<Element>>()
        var pushed: Set<Vertex<Element>> = []
        var visited: [Vertex<Element>] = []
        stack.push(source)
        pushed.insert(source)
        visited.append(source)
        
        outer: while let vertex = stack.peek() { // check top
            let neighbors = edges(from: vertex) // find neighbours
            guard !neighbors.isEmpty else { // no edges
                stack.pop()
                continue
            }
            for edge in neighbors { //
                if !pushed.contains(edge.destination) {
                    stack.push(edge.destination)
                    pushed.insert(edge.destination)
                    visited.append(edge.destination)
                    continue outer //
                } }
            stack.pop() //
        }
        
        return visited
    }
}


//MARK:- Dijkstra's algorithms
public enum Visit<T: Hashable> {
    case start // 1
    case edge(Edge<T>) // 2
}
public class Dijkstra<T: Hashable> {
    public typealias Graph = AdjacencyList<T>
    let graph: Graph
    public init(graph: Graph) {
        self.graph = graph
    }
    private func route(to destination: Vertex<T>,
                       with paths: [Vertex<T> : Visit<T>]) -> [Edge<T>]{
        var vertex = destination // 1
        var path: [Edge<T>] = [] // 2
        while let visit = paths[vertex], case .edge(let edge) = visit {
            path = [edge] + path // 4
            vertex = edge.source // 5
        }
        return path // 6
    }
    
    
    private func distance(to destination: Vertex<T>, with paths: [Vertex<T> : Visit<T>]) -> Double {
        let path = route(to: destination, with: paths) // 1
        let distances = path.compactMap { $0.weight } // 2
        return distances.reduce(0.0, +) // 3
    }
    public func shortestPath(from start: Vertex<T>) -> [Vertex<T>:Visit<T>]{
        var paths: [Vertex<T> : Visit<T>] = [start: .start] // 1
        // 2
        var priorityQueue = PriorityQueue<Vertex<T>>(sort: {
            self.distance(to: $0, with: paths) <
                self.distance(to: $1, with: paths)
        })
        priorityQueue.enqueue(start) // 3
        while let vertex = priorityQueue.dequeue() { // 1
            for edge in graph.edges(from: vertex) { // 2
                guard let weight = edge.weight else { // 3
                    continue
                }
                if paths[edge.destination] == nil ||
                    distance(to: vertex, with: paths) + weight <
                    distance(to: edge.destination, with: paths) { // 4
                    paths[edge.destination] = .edge(edge)
                    priorityQueue.enqueue(edge.destination)
                } }
        }
        return paths
    }
}




// ***************** Queue ****************


public protocol QueueProto {
    associatedtype Element
    mutating func enqueue(_ element: Element)
    mutating func dequeue()->Element?
    var peek:Element?{get}
    var isEmpty:Bool {get}
}
public struct QueueStack<T> : QueueProto {
    /// time complexity: O(1)
    public mutating func enqueue(_ element: T) {
        array.append(element)
    }
    
    /// time complexity: O(n)
    @discardableResult
    public mutating func dequeue() -> T? {
        isEmpty ? nil : array.removeFirst()
    }
    
    public var peek: T? {
        array.first
    }
    
    public var isEmpty: Bool {
        array.isEmpty
    }
    
    public typealias Element = T
    
    private var array:[T] = []
    public init(){}
    
}



// ***************** Stack ****************

public struct StackArr<T> {
    private var array:[T] = []
    public mutating func push(_ value: T){
        array.append(value)
    }
    @discardableResult
    public mutating func pop()->T? {
        array.isEmpty ? nil : array.popLast()
    }
    public var isEmpty:Bool {
        return array.count == 0
    }
    public func peek()->T?{
        array.isEmpty ? nil : array.last
    }
}


// ***************** Priority Queue ****************

///FIXME:- incomplete
public struct PriorityQueue<T> {
    /// time complexity: O(1)
    public mutating func enqueue(_ element: T) {
        array.append(element)
    }
    
    /// time complexity: O(n)
    @discardableResult
    public mutating func dequeue() -> T? {
        isEmpty ? nil : array.removeFirst()
    }
    
    public var peek: T? {
        array.first
    }
    
    public var isEmpty: Bool {
        array.isEmpty
    }
    
    public typealias Element = T
    
    private var array:[T] = []
    private let sort:(Element, Element)->Bool
    public init(sort: @escaping (Element, Element)->Bool){
        self.sort = sort
    }
}
