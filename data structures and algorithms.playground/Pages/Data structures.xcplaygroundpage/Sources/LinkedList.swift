import Foundation
public class ListNode<T> {
    public private (set) var value: T
    public var next: ListNode?
    public init(value: T, next: ListNode? = nil) {
        self.value = value
        self.next = next
    }
//    deinit {
//        print("\(value) deleted...")
//    }
}
extension ListNode{
    public func setValue(_ val:T){
        value = val
    }
    public func printList(){
        var temp:ListNode? = self
        while temp != nil {
            print(temp!.value)
            temp = temp?.next
        }
    }
    public func printDescription(){
        guard let next = next else {
            print("\(value)" , terminator:"")
            return
        }
        print("\(value)" , terminator:"-->")
        next.printDescription()
    }
}

/// linked list
public struct LinkedList<T>{
    public var head: ListNode<T>?
    public var tail: ListNode<T>?
    public init(){}
    public var isEmpty:Bool {
        return head == nil
    }
}
extension LinkedList {
    public var description: String {
        guard let head = head else {
            return "Empty list"
        }
        return String(describing: head)
    }
    public func printList(){
        guard let head = head else {
            return
        }
        head.printDescription()
    }
}

extension LinkedList {
    public mutating func push(_ value: T){
        head = ListNode(value: value, next: head)
        if tail == nil {
            tail = head
        }
    }
}
extension LinkedList {
    public mutating func append(_ value: T){
        let newNode = ListNode(value: value)
        if tail == nil {
            tail = newNode
            head = newNode
        }else {
            tail?.next = newNode
            tail = newNode
        }
    }
}

extension LinkedList {
    public mutating func insert(_ value: T, at index: Int){
        let newNode = ListNode(value: value)
        var currentNode = head
        var currentIndex = 0
        while(currentNode != nil && currentIndex < index){
            currentNode = currentNode?.next
            currentIndex += 1
        }
        /// if no head
        if head == nil {
            head = newNode
            tail = newNode
        }else{
            newNode.next = currentNode?.next
            currentNode?.next = newNode
            if newNode.next == nil {
                /// update tail
                tail = newNode
            }
        }
    }
}

extension LinkedList {
    public mutating func pop()->T?{
        /// base case
        defer {
            head = head?.next
            if isEmpty {
                tail = nil
            }
        }
        return head?.value
    }
}
extension LinkedList {
    public mutating func removeLast()->T?{
        /// if no node
        if head == nil {
            return nil
        }
        if head?.next == nil {
            return pop()
        }
        var current = head
        var prev = head
        
        while let next = current?.next {
            prev = current
            current = next
        }
        prev?.next = nil
        tail = prev
        return current?.value
    }
}

//MARK:- Test
public extension LinkedList where T == Int{
    static func test (){
        var list = LinkedList<T>()
        list.push(3)
        list.push(2)
        list.push(1)
        list.append(4)
        list.push(7)
        list.append(9)
        list.insert(11, at: 4)
        list.printList()
        print()
        printRev(node: list.head)
    }
    static func printRev(node: ListNode<Int>?){
        if node == nil {
            return
        }
        printRev(node: node?.next)
        print(node!.value)
    }
}

/// merge 2 sorted list
public func mergeList<T: Comparable>(list1: LinkedList<T>, list2: LinkedList<T>)->LinkedList<T>{
    var result = LinkedList<T>()
    result.head = mergeNodes(node1: list1.head, node2: list2.head)
    return result
}
private func mergeNodes<T: Comparable>(node1: ListNode<T>?, node2: ListNode<T>?)-> ListNode<T>?{
    if node1 == nil {return node2}
    if node2 == nil {return node1}
    var result: ListNode<T>?
    if node1!.value < node2!.value {
        result = node1
        result?.next = mergeNodes(node1: node1?.next, node2: node2)
    }else{
        result = node2
        result?.next = mergeNodes(node1: node1, node2: node2?.next)
    }
    return result
}
