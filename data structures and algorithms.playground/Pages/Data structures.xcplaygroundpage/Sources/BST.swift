import Foundation

public class BSTNode<T> {
    public var value:T
    public var left: BSTNode?
    public var right: BSTNode?
    public init(_ value: T) {
        self.value = value
    }
    public func inoder(){
        left?.inoder()
        print(value)
        right?.inoder()
    }
}

public struct BSTree<Element: Comparable>{
    public private(set) var root: BSTNode<Element>?
    public init(){}
    public mutating func insert(_ value: Element){
        root = insert(root, value: value)
    }
    public func inorder(){
        root?.inoder()
    }
    private mutating func insert(_ node: BSTNode<Element>?, value: Element)->BSTNode<Element>{
        guard let node = node else {
            return BSTNode(value)
        }
        if node.value > value {
            node.left = insert(node.left, value: value)
        }else{
            node.right = insert(node.right, value: value)
        }
        return node
    }
}



//MARK:- Test
public extension BSTree where Element == Int{
    static func test(){
        var bst = BSTree<Int>()
        bst.insert(11)
        bst.insert(12)
        bst.insert(14)
        bst.insert(1)
        bst.insert(19)
        convertBSTToDLL(root: bst.root)
//        bst.inorder()
    }
}
