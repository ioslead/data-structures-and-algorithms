import Foundation

public struct Heap <Element: Equatable> {
    private var arr:[Element] = []
    private var sortBy:(Element, Element)->Bool
    public init(sortBy: @escaping (Element, Element)->Bool){
        self.sortBy = sortBy
    }
}
public extension Heap {
    var isEmpty:Bool {arr.isEmpty}
    var count:Int {arr.count}
    func peak()->Element?{arr.first}
    func leftChildIndex(ofParentAt index: Int)->Int {2*index+1}
    func rightChildIndex(ofParentAt index: Int)->Int {2*index+2}
    func parentIndex(ofChildAt index: Int)->Int{(index-1)/2}
    mutating func setSortBy(_ sortBy:@escaping (Element, Element)->Bool){
        self.sortBy = sortBy
        
    }
}
public extension Heap {
    private mutating func toggleHeapType(){
        
    }
    mutating func insert(_ element: Element) {
        arr.append(element)
        siftUp(from: arr.count - 1)
    }
    @discardableResult
    mutating func remove() -> Element? {
        guard !isEmpty else {
            return nil
        }
        arr.swapAt(0, count - 1)
        defer {
            siftDown(from: 0)
        }
        return arr.removeLast()
    }
    
    func index(of element: Element, startingAt i: Int) -> Int?{
        if i >= count {
            return nil
        }
        if sortBy(element, arr[i]) {
            return nil
        }
        if element == arr[i] {
            return i
        }
        if let j = index(of: element, startingAt: leftChildIndex(ofParentAt: i)){
            return j
        }
        if let j = index(of: element, startingAt: rightChildIndex(ofParentAt: i)){
            return j
        }
        return nil
    }
    
    @discardableResult
    mutating func remove(at index: Int) -> Element? {
      guard index < arr.count else {
        return nil
      }
      if index == arr.count - 1 {
        return arr.removeLast()
      }else {
        arr.swapAt(index, arr.count - 1)
        defer {
          siftDown(from: index)
          siftUp(from: index)
        }
        return arr.removeLast()
      }
    }
    
    private mutating func siftUp(from index: Int) {
        var child = index
        var parent = parentIndex(ofChildAt: child)
        while child > 0 && sortBy(arr[child], arr[parent]){
            arr.swapAt(child, parent)
            child = parent
            parent = parentIndex(ofChildAt: child)
        }
    }
    private mutating func siftDown(from index: Int) {
        var parent = index
        while true {
            let left = leftChildIndex(ofParentAt: parent)
            let right = rightChildIndex(ofParentAt: parent)
            var candidate = parent
            if left < count && sortBy(arr[left], arr[candidate]){
                candidate = left
            }
            if right < count && sortBy(arr[right], arr[candidate]){
                candidate = right
            }
            if candidate == parent {
                return
            }
            arr.swapAt(parent, candidate)
            parent = candidate
        }
    }
    
    private mutating func shiftDown_rec(from index: Int){
        let left = leftChildIndex(ofParentAt: index)
        let right = rightChildIndex(ofParentAt: index)
        /// check out of bound before accessing values in arr
        if left >= count && right >= count{ return}
        var target = left
        if left < count && right < count {
            target = sortBy(arr[left], arr[right]) ? left : right
        }else if left < count {target = left}
        else{target = right}
        
        if target >= count || target == index {return}
        if sortBy(arr[index], arr[target]){return}
        arr.swapAt(index, target)
        shiftDown_rec(from: target)
    }
    
    private mutating func shiftUp_rec(from index: Int){
        let parent = parentIndex(ofChildAt: index)
        if parent < 0 || index == parent {return}
        if sortBy(arr[parent], arr[index]){return}
        arr.swapAt(parent, index)
        shiftUp_rec(from: parent)
    }
}
