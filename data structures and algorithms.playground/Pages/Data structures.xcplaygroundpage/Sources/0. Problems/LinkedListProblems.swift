import Foundation

///Leetcode 2
public func addTwoNumbers(_ l1: ListNode<Int>?, _ l2: ListNode<Int>?) -> ListNode<Int>? {
    let length1 = length(l1)
    let length2 = length(l2)
    if length1 == 0 {return l2}
    if length2 == 0 {return l1}
    
    var result = l1
    if length1 > length2 {
        addTwoList(result, l2)
    }else{
        result = l2
        addTwoList(result, l1)
    }
    handleCarryReversed(&result, 0)
    return result
}
private func addTwoList(_ l1: ListNode<Int>?, _ l2: ListNode<Int>?){
    var head1 = l1, head2 = l2
    while head2 != nil {
        let sum = head1!.value + head2!.value
        head1?.setValue(sum)
        head1 = head1?.next
        head2 = head2?.next
    }
}

private func handleCarryReversed(_ list: inout ListNode<Int>?, _ carry: Int){ /// first node representing leftmost digit
    guard let node = list else {
        if carry > 0{
            list = ListNode(value: carry)
        }
        return
    }
    let sum = node.value+carry
    let newCarry = sum/10
    let newValue = sum%10
    node.setValue(newValue)
    handleCarryReversed(&node.next, newCarry)
    
}

private func length(_ list: ListNode<Int>?)->Int {
    var ans = 0
    var temp = list
    while temp != nil {
        ans += 1
        temp = temp?.next
    }
    return ans
}

