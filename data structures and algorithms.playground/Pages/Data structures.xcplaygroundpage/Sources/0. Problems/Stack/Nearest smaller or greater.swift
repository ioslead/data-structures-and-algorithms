import Foundation
public extension Array where Element == Int{
    
    //MARK:- nearest smaller in Left
    func nsl()->[Element?]{
        var stack = Stack<Element>()
        var result:[Element?] = []
        for i in 0...self.count-1{
            if(stack.isEmpty){result.append(nil)}
            else if let top = stack.top, top < self[i] {
                result.append(stack.top)
            }else{
                while let top = stack.top, top >= self[i] {
                    stack.pop()
                }
                result.append(stack.top)
            }
            stack.push(self[i])
        }
        return result
    }
    /// index of nearest smaller in Left
    func nsl1()->[Element?]{
        var s = Stack<Element>()
        var result:[Element?] = []
        for i in 0..<count{
            while !s.isEmpty && self[s.top!] >= self[i]{
                s.pop()
            }
            s.isEmpty ? result.append(nil) : result.append(s.top)
            s.push(i)
        }
        return result
    }
    
    //MARK:- nearest smaller in Right
    func nsr()->[Element?]{
        var stack = Stack<Element>()
        var result:[Element?] = []
        for i in (0...self.count-1).reversed(){
            if(stack.isEmpty){result.append(nil)}
            else if let top = stack.top, top < self[i] {
                result.append(stack.top)
            }else{
                while let top = stack.top, top >= self[i] {
                    stack.pop()
                }
                result.append(stack.top)
            }
            stack.push(self[i])
        }
        return result.reversed()
    }
    //MARK:- nearest greater in Left
    func ngl()->[Element?]{
        var stack = Stack<Element>()
        var result:[Element?] = []
        for i in 0...self.count-1{
            if(stack.isEmpty){result.append(nil)}
            else if let top = stack.top, top > self[i] {
                result.append(stack.top)
            }else{
                while let top = stack.top, top <= self[i] {
                    stack.pop()
                }
                result.append(stack.top)
            }
            stack.push(self[i])
        }
        return result
    }
    
    //MARK:- nearest greater in Right
    func ngr()->[Element?]{
        var stack = Stack<Element>()
        var result:[Element?] = []
        for i in (0...self.count-1).reversed(){
            if(stack.isEmpty){result.append(nil)}
            else if let top = stack.top, top > self[i] {
                result.append(stack.top)
            }else{
                while let top = stack.top, top <= self[i] {
                    stack.pop()
                }
                result.append(stack.top)
            }
            stack.push(self[i])
        }
        return result.reversed()
    }
    
    func nsl_Index()->[Int]{
        var stack = Stack<Element>()
        var result:[Int] = []
        for i in 0...self.count-1{
            if(stack.isEmpty){result.append(-1)}
            else if let top = stack.top, self[top] < self[i] {
                result.append(top)
            }else{
                while let top = stack.top, self[top] >= self[i] {
                    stack.pop()
                }
                if let top = stack.top {result.append(top)}
                else {result.append(-1)}
            }
            stack.push(i)
        }
        return result
    }
    func nsr_Index()->[Int]{
        var stack = Stack<Element>()
        var result:[Int] = []
        for i in (0...self.count-1).reversed(){
            if(stack.isEmpty){result.append(self.count)}
            else if let top = stack.top, self[top] < self[i] {
                result.append(top)
            }else{
                while let top = stack.top, self[top] >= self[i] {
                    stack.pop()
                }
                if let top = stack.top {result.append(top)}
                else {result.append(self.count)}
            }
            stack.push(i)
        }
        return result.reversed()
    }
}

//MARK:- Stock span problem:
/// number of days stock price remain equal or smaller to the current day price, including current day.
public extension Array where Element == Int{
    func stockSpan()->[Int]{
        var stack = Stack<Element>()
        var result:[Int] = []
        for i in 0...self.count-1{
            if(stack.isEmpty){result.append(-1)}
            else if let top = stack.top, self[top] > self[i] {
                result.append(top)
            }else{
                while let top = stack.top, self[top] <= self[i] {
                    stack.pop()
                }
                if let top = stack.top {result.append(top)}
                else {result.append(-1)}
            }
            stack.push(i)
        }
        for i in 0...result.count-1{
            result[i] = i-result[i]
        }
        return result
    }
}

//MARK:- Maximum area histogram
public extension Array where Element == Int{
    func mah()->Int{
        return mah_arr().max() ?? 0
    }
    func mah_arr()->[Int]{
        let nsl = nsl_Index()
        let nsr = nsr_Index()
        var result:[Int] = []
        for i in 0...self.count-1{
            result.append((nsr[i]-nsl[i]-1)*self[i])
        }
        return result
    }
}

//MARK:- Maximum area rectangle in binary matrix

public func maxAreaRectInBinaryMatrix(mat:[[Int]])->Int{
    let r:Int = mat.count
    let c:Int = mat.first?.count ?? 0
    var copyMat = mat
    for i in 1...r-1{
        for j in 0...c-1{
            copyMat[i][j] = copyMat[i][j] == 0 ? 0 : copyMat[i][j]+copyMat[i-1][j]
        }
    }
    var ans = 0
    for i in 0...r-1{
        let arr_i = copyMat[i]
        ans = max(ans, arr_i.mah())
        
    }
    return ans
}
