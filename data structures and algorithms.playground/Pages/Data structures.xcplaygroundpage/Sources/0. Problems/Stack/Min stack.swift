import Foundation

//MARK:- with extra space
//TODO:- make below function generic
public struct MinStack {
    public init(){}
    var s = Stack<Int>()
    var ss = Stack<Int>()
    public mutating func push(_ value: Int){
        s.push(value)
        if let top_ss = ss.top, top_ss > value{
            ss.push(value)
        }else if ss.top == nil{
            ss.push(value)
        }
    }
    
    @discardableResult
    public mutating func pop()->Int? {
        let e = s.pop()
        if e == ss.top{
            ss.pop()
        }
        return e
    }
    public func getMin()->Int?{
        ss.top
    }
}



//MARK:- without extra space
public struct EfficientMinStack {
    var s = Stack<Int>()
    var min:Int? = nil
    public init(){}

    public mutating func push(_ value: Int){
        if s.isEmpty {
            min = value
            s.push(value)
            return
        }
        guard let min = min else {return}
        if value < min {
            s.push(2*value-min) /// If you are thinking  (min-value) will also work, it will not if value in much smaller say : value=1, min=4. min-value = 4-1 = 3. Now top = 3, min = 1. There is no way to detect this in pop operation. since top is still greater than (current) min.
            self.min = value
        }else{
            s.push(value)
        }
    }
    
    @discardableResult
    public mutating func pop()->Int? {
        guard !s.isEmpty else {
            min = nil
            return min
        }
        guard let min = min else {return nil}
        if let e = s.top, e <= min {
            let curr_element = min
            self.min = 2*min-e
            s.pop()
            return curr_element
        }
        return s.pop()
        
    }
    public func getMin()->Int?{
       return min
    }
}
