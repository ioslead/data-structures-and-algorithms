import Foundation


 class Solution {
     func removeOuterParentheses(_ S: String) -> String {
        var s = Stack<Character>()
        var arr_str = Array(S)
        var list:[Int] = []
        for i in 0..<arr_str.count {
            if s.isEmpty {
                s.push(arr_str[i])
                list.append(i)
            }else if arr_str[i] == "(" && s.top! == ")"{
                s.pop()
                if s.isEmpty {
                    list.append(i)
                }
            }else if arr_str[i] == ")" && s.top! == "(" {
                s.pop()
                if s.isEmpty {
                    list.append(i)
                }
            }else{
                s.push(arr_str[i])
            }
        }
        var result = ""
        for e in list {
            arr_str[e] = "$"
        }
        for e in arr_str {
            if e != "$" {
                result.append(e)
            }
        }
        return result
    }
}

public func test_Paran() -> String{
    let s = Solution()
    return s.removeOuterParentheses("()()")
}
