import Foundation

struct Pair:Equatable {
    let index:Int
    let value:Int
    static func >(lhs: Pair, rhs: Pair) -> Bool {
        return lhs.value>rhs.value
    }
    static func <(lhs: Pair, rhs: Pair) -> Bool {
        return lhs.value<rhs.value
    }
}

//MARK:- kth smallest
public func kthSmallest(_ arr: [Int], k:Int)->Int?{
    var heap = Heap<Int>(sortBy: >)
    for e in arr {
        heap.insert(e)
        if(heap.count > k){
            heap.remove()
        }
    }
    if heap.count == k {
        return heap.peak()
    }
    return nil
}

//MARK:- kth largest
public func kthLargest(_ arr: [Int], k:Int)->Int?{
    var heap = Heap<Int>(sortBy: <)
    for e in arr {
        heap.insert(e)
        if(heap.count > k){
            heap.remove()
        }
    }
    if heap.count == k {
        return heap.peak()
    }
    return nil
}

//MARK:- k Largest
public func kLargestElements(_ arr: [Int], k:Int)->[Int]{
    var heap = Heap<Int>(sortBy: <)
    var result = [Int]()
    for e in arr {
        heap.insert(e)
        if(heap.count > k){
            heap.remove()
        }
    }
    while let element = heap.remove(){
        result.append(element)
    }
    return result.reversed()
}

//MARK:- nearly sorted: k sorted array
public extension Array where Element == Int{
    mutating func sortByK(_ k:Int){
        var sortedArr:[Int] = []
        var heap = Heap<Int>(sortBy: <)
        for element in self {
            heap.insert(element)
            if heap.count > k, let element = heap.remove() {
                sortedArr.append(element)
            }
        }
        while let e = heap.remove(){
            sortedArr.append(e)
        }
        self = sortedArr
    }
}

//MARK:- k closest numbers
/// input : array, x, k: find k elements which are close to x
public extension Array where Element == Int{
    func kClosestElements(x: Int, k:Int)->[Int]{
        var max_heap = Heap<Pair>(sortBy: >)
        var result:[Int] = []
        for i in 0...self.count-1{
            max_heap.insert(Pair(index: i, value: abs(x-self[i])))
            if max_heap.count > k {
                max_heap.remove()
            }
        }
        while let e = max_heap.remove() {
            result.append(self[e.index])
        }
        return result.reversed()
    }
}

//MARK:- top k frequent numbers
public extension Array where Element == Int{
    func kFrequent(_ k:Int)->[Int]{
        var result:[Int] = []
        var freq_dict:[Int: Int] = [:]
        var min_heap = Heap<Pair>(sortBy: <)
        
        for e in self {
            if let freq = freq_dict[e]{
                freq_dict[e] = freq+1
            }else{
                freq_dict[e] = 1
            }
        }
        for key in freq_dict.keys {
            if let freq = freq_dict[key] {
                min_heap.insert(Pair(index: key, value: freq))
                if min_heap.count > k {
                    min_heap.remove()
                }
            }
        }
        while let e = min_heap.remove(){
            result.append(e.value)
        }
        return result
    }
}

//MARK:- refqueny sort
public extension Array where Element == Int {
    mutating func sortByFrequency(){
        var freq_dict:[Int: Int] = [:]
        var max_heap = Heap<Pair>(sortBy: >)
        var result:[Int] = []

        for e in self {
            if let freq = freq_dict[e]{
                freq_dict[e] = freq+1
            }else{
                freq_dict[e] = 1
            }
        }
        for key in freq_dict.keys {
            if let freq = freq_dict[key] {
                max_heap.insert(Pair(index: key, value: freq))
            }
        }
        while let e = max_heap.remove(){
            result.append(e.index)
        }
        self = result
    }
}

//MARK:- k closest points to origin
public extension Array where Element == CGPoint {
    func kClosestPointTo(_ point: CGPoint, k:Int)->[CGPoint]{
        var result:[CGPoint] = []
        var max_heap = Heap<Pair>(sortBy: >)
        for i in 0...self.count-1{
            max_heap.insert(Pair(index: i, value: Int(getDistanceSquare(p1: point, p2: self[i])))) //FIXME:- give support for CGFloat
            if max_heap.count > k {
                max_heap.remove()
            }
        }
        while let e = max_heap.remove() {
            result.append(self[e.index])
        }
        return result
    }
    private func getDistanceSquare(p1:CGPoint, p2:CGPoint)->CGFloat{
        return (p1.x-p2.x)*(p1.x-p2.x) + (p1.y-p2.y)*(p1.y-p2.y)
    }
}

//MARK:- Half-man coding, connect ropes to minimise cost
public extension Array where Element == Int{
    func minCostToConnectRopes()->Element?{
        var min_heap = Heap<Int>(sortBy: <)
        var cost = 0
        for e in self {
            min_heap.insert(e)
        }
        while min_heap.count > 1, let a = min_heap.remove(), let b = min_heap.remove() {
            cost = cost+a+b
            min_heap.insert(a+b)
        }
        
        return cost
    }
}

//MARK:- sum of elements between k1th smallest and k2th smallest numbers
public extension Array where Element == Int{
    func sumOfElements(_ k1: Element, k2:Element)->Element?{
        guard let a = kthSmallest(self, k: k1) else {return nil}
        guard let b = kthSmallest(self, k: k2) else {return nil}
        var sum = 0
        for e in self {
            if e > a && e < b {
                sum = sum + e
            }
        }
        return sum
    }
}
