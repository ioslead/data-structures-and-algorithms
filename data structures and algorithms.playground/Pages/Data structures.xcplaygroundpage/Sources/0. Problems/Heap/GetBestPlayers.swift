import Foundation
//PS:
/// Given n number of players and their crickect and football ranking. write a method to get(will be removed the player) top player.

public enum MatchType{
    case CRICKET
    case FOOTBALL
}
public struct Player:Equatable{
    let rank_cr:Int
    let rank_ft:Int
    init(_ cr: Int, ft: Int){
        rank_cr = cr
        rank_ft = ft
    }
}

public class Match{
    
    var cr_players = Heap<Player>{p1, p2 in
        return p1.rank_cr < p2.rank_cr
        
    }
    var ft_players = Heap<Player>{p1, p2 in
        return p1.rank_ft < p2.rank_ft
        
    }
    
    func addPlayer(_ player: Player){
        cr_players.insert(player)
        ft_players.insert(player)
    }
    
    @discardableResult
    func getBestOf(_ matchtype: MatchType)->Player?{
        switch matchtype {
        case .CRICKET:
            if let cr_player = cr_players.remove(),  let indexInFT = ft_players.index(of: cr_player, startingAt: 0) {
                ft_players.remove(at: indexInFT)
                return cr_player
            }
        case .FOOTBALL:  if let ft_player = ft_players.remove(),  let indexInCR = cr_players.index(of: ft_player, startingAt: 0) {
            cr_players.remove(at: indexInCR)
            return ft_player
        }
        }
        return nil
    }
}


//MARK:- Test
public extension Match {
    static func test(){
        let p1 = Player(1, ft: 5)
        let p2 = Player(2, ft: 3)
        let p3 = Player(3, ft: 2)
        let p4 = Player(4, ft: 1)
        let p5 = Player(5, ft: 4)

        let match = Match()
        match.addPlayer(p1)
        match.addPlayer(p2)
        match.addPlayer(p3)
        match.addPlayer(p4)
        match.addPlayer(p5)


        match.getBestOf(.CRICKET)
        print(match.cr_players)
        print(match.ft_players)

        match.getBestOf(.FOOTBALL)
        print(match.cr_players)
        print(match.ft_players)

    }
}
