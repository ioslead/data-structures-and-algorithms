import Foundation

public func convertBSTToDLL<T>(root: BSTNode<T>?){
    if root == nil {return}
    var prev:BSTNode<T>? = nil
    var head:BSTNode<T>? = nil
    
    func inorder(_ node: BSTNode<T>?){
        if node == nil {return}
        inorder(node?.left)
        if prev == nil{
            head = node
        }else{
            prev?.right = node
            node?.left = prev
        }
        prev = node
        inorder(node?.right)
    }
    inorder(root)
    printDLLTree(head)
}

private func printDLLTree<T>(_ node: BSTNode<T>?){
    if node == nil {return}
    print(node!.value)
    printDLLTree(node?.right)
}


/// print all paths from root to leaves
public func printPaths<T>(_ node: BNode<T>?, _ path: String?){
    guard let node = node else {
        return
    }
    var newPath = "\(node.value)"
    if let path = path {
        newPath = path+"\t"+newPath
    }
    if node.left == nil && node.right == nil {
        print(newPath)
    }
    printPaths(node.left, newPath)
    printPaths(node.right, newPath)
}

/// mirror tree
public func convertToMirrorTree<T>(_ node:BNode<T>?){
    if node == nil {return}
    convertToMirrorTree(node?.left)
    convertToMirrorTree(node?.right)
    let temp = node?.left
    node?.left = node?.right
    node?.right = temp
}

/// child sum property: A binary tree having all its nodes value is such that node.value == node.left.value + node.right.value. Leaf nodes will be considerd as true.
public func hasChildSumProperty(_ node: BNode<Int>?)->Bool{
    guard let node = node else {return true}
    if node.left == nil && node.right == nil {return true}
    var childSum = 0
    if let left = node.left {
        childSum += left.value
    }
    if let right = node.right{
        childSum += right.value
    }
    if childSum != node.value {
        //early exit
        return false
    }
    return hasChildSumProperty(node.left) && hasChildSumProperty(node.right)
}

public extension BNode where T == Int{
    func maxSumLeafToLeaf()->Int{
        var ans = 0
        @discardableResult
        func maxSum(_ node: BNode?)->Int{
            guard let node = node else {return 0}
            if node.left == nil && node.right == nil {return node.value}
            let ls = maxSum(node.left)
            let rs = maxSum(node.right)
            ans = max(ans, ls+rs+node.value)
            return node.value+max(ls, rs)
        }
        //call
        maxSum(self)
        return ans
    }
    func maxSumNodeToNode()->Int{
        var ans = 0
        @discardableResult
        func maxSum(_ node: BNode?)->Int{
            guard let node = node else {return 0}
            if node.left == nil && node.right == nil {return node.value}
            let ls = maxSum(node.left)
            let rs = maxSum(node.right)
            ans = max(ans, ls+rs+node.value)
            let subtreeSum = max(max(ls,rs)+node.value, node.value)
            return max(subtreeSum, 0)
        }
        //call
        maxSum(self)
        return ans
    }
    /// check if path  from root for a given sum exist
    func findSumPath(_ sum:Int)->Bool{
        if self.value == sum {return true}
        
        var ans = false
        if let left = self.left {
            ans = ans || left.findSumPath(sum-self.value)
        }
        if let right = self.right{
            ans = ans || right.findSumPath(sum-self.value)
        }
        return ans
    }
    
    /// vertical sum
    func verticalSum()->[Int:Int]{
        var dict:[Int:Int] = [:]
         func verticalSumUtil(_ node: BNode<Int>?, _ index: Int){
            guard let node = node else {return}
            if let value = dict[index] {
                dict[index] = value + node.value
            }else{
                dict[index] =  node.value
            }
            verticalSumUtil(node.left, index-1)
            verticalSumUtil(node.right, index+1)
        }
        //call
        verticalSumUtil(self, 0)
        return dict
    }
    
}
/// check if path  from root for a given sum exist
public func checkSumPath<T: Numeric>(_ root: BNode<T>?, _ sum:T)->Bool {
    if root == nil {return sum == 0}
    let diff = sum - root!.value
    if diff == 0 {return true}
    return checkSumPath(root?.left, diff) || checkSumPath(root?.right, diff)
}

public extension BSTree where Element==Int{
    func inorderSuccessorBSTNode(_ node: BSTNode<Element>?)->BSTNode<Element>?{
        guard let node = node else {return nil}
        if let right =  node.right {
            return getMin(right)
        }else{
            var temp = root
            var ans:BSTNode<Element>?
            while temp != nil{
                if temp!.value > node.value{
                    ans = temp
                    temp = temp?.left
                }else{
                    temp = temp?.right
                }
            }
            return ans
        }
        
    }
    private func getMin(_ node: BSTNode<Element>)->BSTNode<Element>?{
        var temp:BSTNode<Element>? = node
        while temp != nil {
            temp = temp?.left
        }
        return temp
    }
    func kthSmallestInBST(_ k:Int)->Element?{
        var count = 0, ans:Element?
        
        func inorder(_ node: BSTNode<Element>?){
            if node == nil {return}
            inorder(node?.left)
            count += 1
            if count == k {ans = node?.value;return}
            inorder(node?.right)
        }
        //call
        inorder(root)
        return ans
    }
    func printAncestors(_ key:Element){
        printAncestors(root, key)
    }
    @discardableResult
    private func printAncestors(_ node: BSTNode<Element>?, _ key:Element)->Bool{
        if node == nil {return false}
        if node!.value == key {return true}
        if printAncestors(node?.left, key) || printAncestors(node?.right, key){
            print(node!.value)
            return true
        }
        return false
    }
}

/// generate binary  tree from inorder and preorder
public func createBTFrom(_ inorder:[Int], _ preorder:[Int], _ pre_index: inout Int)->BNode<Int>?{
    print("pre_index: ", pre_index)
    if pre_index >= preorder.count {return nil}
    var root:BNode<Int>?
    let rootValue = preorder[pre_index]
    if let in_index = inorder.getIndex(rootValue){
        root = BNode(rootValue)
        print("in_index: ", in_index)
        if inorder.count == 1 {return root}
        pre_index += 1
        root?.left = createBTFrom(Array(inorder[0..<in_index]), preorder, &pre_index)
        root?.right = createBTFrom(Array(inorder[in_index+1..<inorder.count]), preorder, &pre_index)
    }else{
        pre_index += 1
    }
    return root
}

extension Array where Element == Int{
    func getIndex(_ element: Element)->Int?{
        for i in 0..<count{
            if self[i] == element {return i}
        }
        return nil
    }
}
public extension BNode {
    func printAllLeaves(){
        if left == nil && right == nil {
            print(value)
            return
        }
        left?.printAllLeaves()
        right?.printAllLeaves()
    }
    
    func printLeftView(){ ///CHECK:- calling this mrthod prints correct result but doesn't executes next statements written after call
        var queue = QueueArray<Any>()
        queue.enQueue("$")
        queue.enQueue(self)
        while !queue.isEmpty {
            if let element = queue.deQueue(){
                if let element =  element as? String{
                    queue.enQueue(element)
                    if let front = queue.peek as? BNode{
                        print(front.value)
                    }
                }else if let element = element as? BNode {
                    if let leftNode = element.left {queue.enQueue(leftNode)}
                    if let  rightNode = element.right {queue.enQueue(rightNode)}
                }
            }
        }
    }
    func printLeftViewRec(){
        var maxLevel = 0
        
        func printLevelUtil(_ node: BNode?, _ level: Int){
            guard let node = node else {
                return
            }
            if maxLevel < level {
                print(node.value)
                maxLevel = level
            }
            printLevelUtil(node.left, level+1)
            printLevelUtil(node.right, level+1)
        }
        //call
        printLevelUtil(self, 1)
    }
    func printRightViewRec(){
        var maxLevel = 0
        
        func printLevelUtil(_ node: BNode?, _ level: Int){
            guard let node = node else {
                return
            }
            if maxLevel < level {
                print(node.value)
                maxLevel = level
            }
            printLevelUtil(node.right, level+1)
            printLevelUtil(node.left, level+1)
        }
        //call
        printLevelUtil(self, 1)
    }
    
    /// vertical sum
    func printTopView(){
        var dict:[Int:(T, Int)] = [:] //value, height
        func printTopViewUtil(_ node: BNode<T>?, _ index: Int, _ height: Int){
            guard let node = node else {return}
            if let element = dict[index] {
                if  height < element.1 {
                    dict[index] =  (node.value, height)
                }
            }else {
                dict[index] =  (node.value, height)
            }
            printTopViewUtil(node.left, index-1, height+1)
            printTopViewUtil(node.right, index+1, height+1)
        }
        //call
        printTopViewUtil(self, 0, 0)
//        return dict
        print(dict)
        for e in  dict {
            print(e.0)
        }
    }
}

public func createTreeFrom(_ inorder:[Int], _ preorder:[Int])->BNode<Int>?{
    /// basecase
    if inorder.count == 0 {return nil}
    if inorder.count == 1 {
        return BNode(inorder[0])
    }
    var index = 0
    label:
    for e in preorder {
        for i in 0..<inorder.count{
            if inorder[i] == e {index = i;break label}
            
        }
    }
    let node = BNode(inorder[index])
    node.left = createTreeFrom(Array(0..<index),preorder)
    node.right = createTreeFrom(Array(index+1..<inorder.count-1), preorder)

    return nil
}
