import Foundation

public protocol Queue {
    associatedtype Element
    mutating func enQueue(_ element: Element)
    mutating func deQueue()->Element?
    var peek:Element?{get}
    var isEmpty:Bool {get}
}

public struct QueueArray<T> : Queue {
    /// time complexity: O(1)
    public mutating func enQueue(_ element: T) {
        array.append(element)
    }
    
    /// time complexity: O(n)
    @discardableResult
    public mutating func deQueue() -> T? {
        isEmpty ? nil : array.removeFirst()
    }
    
    public var peek: T? {
        array.first
    }
    
    public var isEmpty: Bool {
        array.isEmpty
    }
    
    public typealias Element = T
    
    private var array:[T] = []
    public init(){}
    
}
extension QueueArray: CustomStringConvertible {
    public var description: String {
        return array.description
    }
}


//MARK:- Test
public extension QueueArray where T == String {
    static func test(){
        var queue = QueueArray<T>()
        queue.enQueue("Ray")
        queue.enQueue("Brian")
        queue.enQueue("Eric")
        print(queue.description)
        queue.deQueue()
        _ = queue.peek
        print(queue.description)
    }
}

/// Impliment queue with stack
public struct QueueWithStack<T>{
    var s1 = Stack<T>()
    var s2 = Stack<T>()
    public mutating func enQueue(_ e: T){
        while let popped = s2.pop() {
            s1.push(popped)
        }
        s1.push(e)
    }
    public mutating func deQueue()->T?{
        while let popped = s1.pop() {
            s2.push(popped)
        }
        return s2.pop()
    }
    public init(){}
}
