import Foundation
//Test_codeble()
//print(numbers_str)
//numbers_str_compact
//only_nums
let res = [2,3,1, nil].mySimpleCompactMap { (e) -> (String?) in
    if let e = e {return "\(e*e)"} else {return nil}
}
//print(res)

let rs = [3,6,nil].myCompactMap{ (e) -> (String?) in
    if let e = e {return "\(e*e)"} else {return nil}
}
//print(rs)

func isAbr(_ word1:String, _ word2: String)->Bool{
    var newWord = ""
    var freq = 0
    word2.forEach { (char) in
        
        if let number = Int(String(char)) {
            freq = freq == 0 ? number : freq*10+number

        }else{
            if freq != 0 {
                for _ in 1...freq{
                    newWord += "$"
                }
            }
            newWord += String(char)
            freq = 0
        }
    }
    if freq != 0 {
        for _ in 1...freq{
            newWord += "$"
        }
    }

    if newWord.count != word1.count {return false}
    let word_arr1 = Array(word1)
    let word_arr2 = Array(newWord)
    for i in 0..<word1.count{
        if word_arr2[i] != "$" {
            if word_arr2[i] != word_arr1[i] {return false}
        }
    }
    return true
}
//isAbr("internationalization", "i5a11o1")
//
//isAbr("internationalization", "i12iz4n")
//isAbr("apple", "a2e")

//func SIP(numberOfMonths:Int, investmentPerMonth:Double, rate: Double)->Double{
