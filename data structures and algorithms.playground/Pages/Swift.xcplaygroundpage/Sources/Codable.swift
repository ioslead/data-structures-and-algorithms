import Foundation
/// conform to Decodable  to read data of a given type.
/// conform to Encodable  to  encoding of data



/// handling diffent key names

let error_json:[String: Any] = [
    "error_code": 404,
    "error_message": "file not found",
    "one":1,
    "two":2
]
let failure_json:[String: Any] = [
    "failure_code": 404,
    "failure_message": "file not found",
    "one":1,
    "two":2
]

public extension Decodable {
    init(from: Any) throws {
        let data = try JSONSerialization.data(withJSONObject: from, options: .prettyPrinted)
        let decoder = JSONDecoder()
        self = try decoder.decode(Self.self, from: data)
    }
}


struct CommonModel : Decodable {
    var code: Int?
    var message: String?
    var one:Int //common in both
    var two:Int? //common in both

    private enum CodingKeys: String, CodingKey{
        case one, two
    }
    private enum Error_CodingKeys : String, CodingKey {
        case  code = "error_code", message = "error_message"
    }
    private enum Failure_CodingKeys : String, CodingKey {
        case  code = "failure_code", message = "failure_message"
    }
    init(from decoder: Decoder) throws {
        let commonValues =  try decoder.container(keyedBy: CodingKeys.self)
        let errors = try decoder.container(keyedBy: Error_CodingKeys.self)
        let failures = try decoder.container(keyedBy: Failure_CodingKeys.self)

        ///common
        self.one = try commonValues.decodeIfPresent(Int.self, forKey: .one)!
        self.two = try commonValues.decodeIfPresent(Int.self, forKey: .two)
        /// different
        if errors.allKeys.count > 0{
            self.code = try errors.decodeIfPresent(Int.self, forKey: .code)
            self.message = try errors.decodeIfPresent(String.self, forKey: .message)
        }
        if failures.allKeys.count > 0{
            self.code = try failures.decodeIfPresent(Int.self, forKey: .code)
            self.message = try failures.decodeIfPresent(String.self, forKey: .message)
        }
    }
}

/// testing
public func Test_codeble(){
    do {
        let err_obj = try CommonModel(from: error_json)
        print(err_obj)
        let failed_obj = try CommonModel(from: failure_json)
        print(failed_obj)
        
    }catch let error {
        print(error.localizedDescription)
    }
}
