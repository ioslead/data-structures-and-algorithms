import Foundation
/*
 Higher order functions are functions that takes another function/closure as an argument or the functions which return type is also a function.
 */

let numbers:[Int?] = [1,2,3,4, nil, 5, nil]

//Map
/// Map can be used to loop over a collection and apply the same operation to each element in the collection.
public let numbers_str = numbers.map { (n) -> String? in   /// ["1", "2", "3", "4", nil, "5", nil]
    if let n = n {return "\(n)"}else {return nil}
    
}

//CompactMap
///CompactMap is the same as the Map function but with optional handling capability. Use this method to receive an array of non optional values when your transformation produces an optional value.
public let numbers_str_compact = numbers.compactMap { (n) -> String? in /// ["1", "2", "3", "4","5"]
    if let n = n {return "\(n)"}else {return nil}
}


let any_nums = [3,4,2,"3",1,"11"] as [Any]
public let only_nums = any_nums.compactMap { (e) -> Int? in
    if let e = e as? Int {
        return e
    }
    return nil
}

public extension Array {
    func myCompactMap<T>(_ completion:(Element) throws ->(T?)) rethrows ->[T]{
        var res:[T] = []
        try self.forEach{ e in
            let value = try completion(e)
            if let value = value {res.append(value)}
        }
        return res
    }
    func mySimpleCompactMap<T>(_ complection: (Element)->T?)->[T]{
        var res:[T] = []
        self.forEach { (element) in
            if let mappedElement = complection(element){
                res.append(mappedElement)
            }
        }
        return res
    }
    func mySipmleMap<T>(_ complection: (Element?)->T)->[T]{
        var res:[T] = []
        self.forEach { (element) in
            res.append(complection(element))
        }
        return res
    }
}
