///     For problem description visit [leetcode problem #121](https://leetcode.com/problems/best-time-to-buy-and-sell-stock/)
///


public func maxProfit(_ prices: [Int]) -> Int {
    let n = prices.count
    /// base case
    if n < 2 {return 0}
    var buy:[Int] = Array(repeating: prices[0], count: n)
    var sell:[Int] = Array(repeating: prices[n-1], count: n)
    for i in 1...n-1 {
        buy[i] = min(buy[i-1], prices[i])
    }
    for i in (0...n-2).reversed() {
        sell[i] = max(sell[i+1], prices[i])
    }
    
    var max = sell[0] - buy[0]
    for i in 1...n-1 {
        let diff = sell[i] - buy[i]
        if diff > max {
            max = diff
        }
    }
    return max
}
