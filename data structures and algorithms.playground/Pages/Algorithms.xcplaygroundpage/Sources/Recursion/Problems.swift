import Foundation

/// input: ABC
/// insert "-"
/// output: A-B-C, AB-C, A-BC, ABC

public extension String {
    mutating func printPermutationWithSpace(){
        let first_char = String(self.first!)
        self.remove(at: self.startIndex)
        permutationWithSpaceUtil(ip: self, op: first_char)
    }
    private mutating func permutationWithSpaceUtil(ip:String, op:String){
        /// base case
        if ip.count == 0 {
            print(op)
            return
        }
        /// process 1st character
        let first = String(ip.prefix(1))
        let remaining = String(ip.suffix(ip.count-1))
        /// with "-" included
        permutationWithSpaceUtil(ip: remaining, op: op+"-"+first)
        /// with "-"  excluded
        permutationWithSpaceUtil(ip: remaining, op: op+first)
    }
}
