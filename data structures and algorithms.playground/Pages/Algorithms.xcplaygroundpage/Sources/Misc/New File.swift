import Foundation


extension Int {
    func withCommas() -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal
        return numberFormatter.string(from: NSNumber(value:self))!
    }
}


public func recoverLoss(numberOfMonths:Int, investmentPerMonth:Double, profitRatePerMonth: Double)->Double{
    var ans:Double = investmentPerMonth + investmentPerMonth*profitRatePerMonth
    if numberOfMonths < 1 {return investmentPerMonth}
    for _ in 1..<numberOfMonths {
        let total = ans + investmentPerMonth
        ans = total + total*profitRatePerMonth
    }
    return ans
}
public func recoverLossTest(){
    print("month end", "\t\t","amount")
    for i in 0...360{
        print(i, "\t\t\t", Int(recoverLoss(numberOfMonths:i, investmentPerMonth: 1000, profitRatePerMonth: 0.02)))
    }
}

public func recoverLossTest1(){
    print("month #", "\t\t\t","amount")
//    let oneSpace = " ", twoSpace = "  "
//    for i in [0,1,2,3,4,12,24,60,120,240,300,360] {
//        let month = i < 10 ? twoSpace+"\(i)": i < 100 ? oneSpace+"\(i)" : "\(i)"
//
//        print("\(month)                     ", Int(recoverLoss(numberOfMonths:i, investmentPerMonth: 1000, rate: 0.02)).withCommas())
//    }
    for i in 0...360{
        print(i, "\t\t\t", Int(recoverLoss(numberOfMonths:i, investmentPerMonth: 1000, profitRatePerMonth: 0.02)))
}
}
