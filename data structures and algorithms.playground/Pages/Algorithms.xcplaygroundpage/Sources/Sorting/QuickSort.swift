import Foundation

public extension Array where Element == Int{
    mutating func quickSort(){
        quickSortUtil(s: 0, e: count-1)
    }
    mutating func quickSortUtil(s: Int, e: Int){
        /// base case
        if s >= e {return}
        let pivot = divideAt(s: s, e: e)
        quickSortUtil(s: s, e: pivot-1)
        quickSortUtil(s: pivot+1, e: e)

    }
    private mutating func divideAt(s: Int, e:Int)->Int {
        let pivot = self[e]
        var j = s-1
        for i in s..<e {
            if self[i] <= pivot {
                j += 1 /// increment index of smaller element
                if i != j {self.swapAt(i, j)}
            }
        }
        j += 1
        self.swapAt(e, j)
        return j
    }
}
