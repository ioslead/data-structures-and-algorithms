import Foundation

public extension Array where Element == Int {
    mutating func  mergeSort(){
        let s = 0
        let e = count-1
        self = divide(s, e)
    }
    @discardableResult
    private mutating func divide(_ s: Int, _ e:Int)->[Element]{
        /// base case
        if s > e {return []}
        if s == e {return [self[s]]}
        /// divide
        let mid = s + (e-s)/2
        let leftSorted = divide(s, mid)
        let rightSrted = divide(mid+1, e)
        /// conquer
        return merge(arr1: leftSorted, arr2: rightSrted)
    }
    private mutating func merge(arr1:[Element], arr2:[Element])->[Element]{
        var res:[Element] = []
        var count1 = 0, count2 = 0
        while count1 < arr1.count && count2 < arr2.count {
            if arr1[count1] < arr2[count2]{
                res.append(arr1[count1])
                count1 += 1
            }else{
                res.append(arr2[count2])
                count2 += 1
            }
        }
        /// append rest
        while count1 < arr1.count {
            res.append(arr1[count1])
            count1 += 1
        }
        while count2 < arr2.count {
            res.append(arr2[count2])
            count2 += 1
        }
        /// return merged sorted array
        return res
    }
}
