import Foundation

/// inplace rotation on square matrix

public func rotateLeft(_ mat: inout [[Int]]){
    let n = mat.count
    for i in 0..<n/2{
        for j in i..<n-1-i{
            let temp = mat[i][j]
            ///for  index : curr_i = prev_j, curr_j = n-1-prev_i
            mat[i][j] = mat[j][n-1-i]
            mat[j][n-1-i] = mat[n-1-i][n-1-j]
            mat[n-1-i][n-1-j] = mat[n-1-j][i]
            mat[n-1-j][i] = temp
        }
    }
}

/// Leetcode 48
public func rotateRight(_ mat: inout [[Int]]){
    let n = mat.count
    for i in 0..<n/2{
        for j in i..<n-1-i{
            let temp = mat[i][j]
            ///for  index : curr_i = n-1-prev_j, curr_j = prev_i
            mat[i][j] = mat[n-1-j][i]
            mat[n-1-j][i] = mat[n-1-i][n-1-j]
            mat[n-1-i][n-1-j] = mat[j][n-1-i]
            mat[j][n-1-i] = temp
        }
    }
}
