
///     [leetcode #1](https://leetcode.com/problems/two-sum/)
///
class Solution {
    
    //MARK:- solution1: Hash
     func twoSum(_ nums: [Int], _ target: Int) -> [Int] {
        var dict:[Int:Int] = [:]
         for (index, num) in nums.enumerated() {
           if let t = dict[target-num] {
               return [t, index]
           }
           dict[num] = index
       }
         return []
    }
    
    //MARK:- solution2: sorting
    func twoSum1(_ nums: [Int], _ target: Int) -> [Int] {
           let sortedArr = nums.sorted()
           var i=0, j=sortedArr.count-1
           while(i<j){
               let tempSum = sortedArr[i]+sortedArr[j];
               if tempSum == target {
                   return getIndices(nums, sortedArr[i], sortedArr[j])
               }
               if tempSum < target {
                   i += 1
               }
               else{
                   j -= 1
               }
           }
           return []
       }
       private func getIndices(_ arr:[Int], _ a:Int, _ b:Int) -> [Int] {
           var ans:[Int] = []
           for (index, item) in arr.enumerated() {
               if item == a {
                   ans.append(index)
                   break
               }
           }
           /// second index
           var j = arr.count-1
           while(j>=0){
               if arr[j] == b{
                   ans.append(j)
                   break
               }
               j -= 1
           }
          
           return ans
       }
}
