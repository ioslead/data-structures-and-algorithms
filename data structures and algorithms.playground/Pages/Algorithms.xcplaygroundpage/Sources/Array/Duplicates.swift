import Foundation


public extension Array where Element == Int{
    func maxContinuous1s()->Int{
        var ans = 0
        var temp = 0
        for e in self {
            if e == 1 {
                temp += 1
                ans = Swift.max(ans, temp)
            }else{
                temp = 0
            }
        }
        return ans
    }
    func findMejority()->Element?{
        var ans = self.first
        if count < 2 {return ans}
        var counter = 1
        for i in 1..<count{
            if self[i] == ans {counter += 1}
            else{
                counter -= 1
                if counter == 0 {
                    ans = self[i]
                    counter = 1
                }
            }
        }
        return ans
    }
    func duplicatesExistWithinDistance(_ k:Int)->Bool{
        var dict:[Element:Int] = [:]
        for i in 0..<count{
            if let index = dict[self[i]]{
                if i-index <= k {return true}
            }
            dict[self[i]] = i
        }
        return false
    }
    mutating func removeDuplicatesInSortedArr(){
        var i=0
        //        if count < 2 {return}
        //        for j in 1..<count{
        //            if self[i] != self[j]{
        //                i += 1
        //                self[i] = self[j]
        //            }
        //        }
        //        self = Array(self[0...i])
        for num in self where num != self[i]{
            i += 1
            self[i] = num
        }
        self = Array(self[0...i])
    }
    mutating func removeDuplicatesAtMostTwiceAllowed(){
        if count <= 2 {return}
        var i = 1
        //        for j in 2..<count{
        //            if self[j] != self[i] {
        //                i += 1
        //                self[i] = self[j]
        //            }
        //            if self[i] == self[j] && self[j] != self[j-1]{
        //                i += 1
        //                self[i] = self[j]
        //            }
        //        }
        //        [1,1,1,2,2,3,3]
        for j in 2..<count{
            if self[i] != self[i-1] || self[j] != self[i]{
                i += 1
                self[i] = self[j]
            }
        }
        self = Array(self[0...i])
    }
}

public func findIntersectArr<T:Hashable>(_ arr1: [T], _ arr2:[T])->[T]{
    var result:[T] = []
    var dict:[T:Int] = [:]
    for e in arr1 {
        if let freq = dict[e]{
            dict[e] = freq+1
        }else{
            dict[e] = 1
        }
    }
    for e in arr2 {
        if let freq = dict[e]{
            result.append(e)
            dict[e] = freq == 1 ? nil : freq-1
        }
    }
    return result
}
