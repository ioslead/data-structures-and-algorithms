import Foundation


/// These type of problems has a knapsack/container/capacity are given and your task is to fill the knapsack with given items to make maximum profit. On the basis of item supply this problem is further categories in 2 parts:
/// 1. 0-1 knapsack problem, where you can pick an item only once. (Limited supply)
/// 2. Unbounded knapsack, where you can pick same item more than once. (Unlimited supply)
///
/// Problems based on 0-1 knapsack:
///
///

/// 1. Subset sum:
/// PS: Given an array of integers ans a sum,  k. You have to find if there is a subset/subarray which sum is equal to given sum, k.
/// i/p: array of integers, k (sub-array sum)
/// o/p: bool

/// recursive solution
func subsetSum(_ arr:[Int], _ n:Int, _ k: Int )->Bool{
    //basecase
    if k==0 {return true}
    if  n==0 {return false}
    
    if arr[n-1] > k {return subsetSum(arr, n-1, k)}
    return subsetSum(arr, n-1, k) || subsetSum(arr, n-1, k-arr[n-1])
}

// DP solution
func subsetSum(arr:[Int], n:Int, k: Int )->Bool{
    /// basecase
    if k==0 {return true}
    if  n==0 {return false}
    return false

}


//MARK:- recursive
public func knapsack_rec(_ wt:[Int], _ pt:[Int], _ n: Int, _ W:Int)->Int{
    /// base case
    if n == 0 {return 0}
    if W == 0 {return 0}
    
    if W < wt[n-1] { /// must be excluded
        return knapsack_rec(wt, pt, n-1, W)
    }else{
        /// can either be included or excluded
        return max(knapsack_rec(wt, pt, n-1, W-wt[n-1])+pt[n-1], knapsack_rec(wt, pt, n-1, W))
    }
}

//MARK:- dp
public func knapsack_dp(_ wt:[Int], _ pt:[Int], _ n: Int, _ W:Int)->Int{
    var t = Array(repeating: Array(repeating: 0, count: W+1), count: n+1)
    for i in 1...n {
        for j in 1...W { /// j is capacity
            if wt[i-1] > j{
                t[i][j] = t[i-1][j]
            }else{
                t[i][j] = max(pt[i-1]+t[i-1][j-wt[i-1]], t[i-1][j])
            }
        }
    }
    return t[n][W]
}

//MARK:- Problems based on 0-1 knapsack

///equal sum partition
public func equalSumPartition(_ arr:[Int])->Bool{
    var s = 0
    arr.forEach{s = s+$0}
    if s%2 == 0 {
        //return subsetSum_rec(arr, arr.count, s/2)
        return subsetSum_dp(arr, arr.count, s/2)
    }
    return false
}

///subset sum
public func subsetSum_rec(_ arr:[Int], _ n: Int, _ S:Int)->Bool{
    if S == 0 {return true}
    if n==0 {return false} /// don't forget to add this otherwise index out of bound may occur
    if arr[n-1] > S {return false}
    return subsetSum_rec(arr, n-1, S-arr[n-1]) || subsetSum_rec(arr, n-1, S)
    
}
public func subsetSum_dp(_ arr:[Int], _ n: Int, _ S:Int)->Bool{
    var t = Array(repeating: Array(repeating: false, count: S+1), count: n+1)
    for i in 0...n{t[i][0] = true}
    for i in 1...n{
        for j in 1...S{
            if arr[i-1] > j  {t[i][j] = t[i-1][j]}
            else {
                t[i][j] = t[i-1][j-arr[i-1]] || t[i-1][j]
            }
        }
    }
    return t[n][S]
}

///number of subset sum
public func countSubsetSum(_ arr:[Int], sum:Int)->Int{
    let n = arr.count
    if sum == 0 {return 1}
    if n == 0 {return 0}
    var t = Array(repeating: Array(repeating: 0, count: sum+1), count: n+1)
    for i in 0...n{
        t[i][0] = 1
    }
    for i in 1...n{
        for  j in 1...sum {
            if arr[i-1] > j {
                t[i][j] = t[i-1][j]
            }else{
                t[i][j] = t[i-1][j-arr[i-1]] + t[i-1][j]
            }
        }
    }
    return t[n][sum]
}

///2 subsets with mininum sum difference
/// divide the elements in two subset such that differance of there sum is minimum, return the difference
public func minimumSubsetSumDifference(_ arr:[Int])->Int{
    let n = arr.count
    var sum = 0
    for i in 0..<n {
        sum += arr[i]
    }
    var t = Array(repeating: Array(repeating: false, count: sum+1), count: n+1)
    for i in 0...n{t[i][0] = true}
    for i in 1...n{
        for j in 1...sum {
            if arr[i-1] > j  {t[i][j] = t[i-1][j]}
            else {
                t[i][j] = t[i-1][j-arr[i-1]] || t[i-1][j]
            }
        }
    }
    var half = sum/2
    while half >= 0{
        if t[n][half] {return sum - 2*half}
        half -= 1
    }
    return Int.max
}

///number of subsets with given difference
public func numberOfSubsetWithGivenDifference(_ arr:[Int], diff: Int)->Int{
    /// divide the elements in two subset such that differance of there sum is minimum, return the difference
    let diff = abs(diff)
    let n = arr.count
    var sum = 0
    for i in 0..<n {
        sum += arr[i]
    }
    
    let s1 = (diff+sum)/2 ///  s1 ~ s2 = diff; s1+s2=sum; ==> s1 = (diff+sum)/2. i.e. if s1 is present then remaining elements sum will be s2
    return countSubsetSum(arr, sum: s1)
}


/// target sum : e.g. Find number of ways +/- sign can be put infront of elements such that their total equals a given target.
/// This is another way of asking numberOfSubsetWithGivenDifference, where difference is the target itself.

// let ans = numberOfSubsetWithGivenDifference([1,1,2,3], diff: 1)

