import Foundation

///The main difference between 0-1 i.e. bounded and and unbounded knapsack is: in unbounded knapsack multiple occurences of an item is allowed.
///Let's suppose in 0-1 knapsack the problem statement is same but items supply in unlimited i.e. selected item can be picked multiple times in beneficial

public func unboundedKnapsack(_ wt:[Int], _ pt:[Int], _ n:Int, _ W:Int)->Int {
    /// basecase
    if n == 0 || W == 0 {return 0}
    
    /// solve
    if wt[n-1] > W {
        return unboundedKnapsack(wt, pt, n-1, W)
    }else{
       return max(
        /// included
        unboundedKnapsack(wt, pt, n, W-wt[n-1])+pt[n-1],
        /// excluded
        unboundedKnapsack(wt, pt, n-1, W)
        )
    }
}
public func unboundedKnapsack_dp(_ wt:[Int], _ pt:[Int], _ n:Int, _ W:Int)->Int {
    var t = Array(repeating: Array(repeating: 0, count: W+1), count: n+1)
    for i in 0...n {
        for  j in 0...W {
            if i == 0 || j == 0 {t[i][j] = 0}
            else if wt[i-1] > j{t[i][j] = t[i-1][j]}
            else {
                t[i][j] = max(
                    t[i][j-wt[i-1]]+pt[i-1]
                    ,
                    t[i-1][j]
                )
            }
        }
    }
    return t[n][W]
}

public func sumKNumbers(_ arr: [Int], _ n: Int, _ sum:Int, _ count:Int)->Bool{
    ///base case
    if sum == 0 {
        return count == 0
    }
    if n == 0 {return false}
    
    if arr[n-1] > sum {return sumKNumbers(arr, n-1, sum, count)}
    else{
        return sumKNumbers(arr, n-1, sum, count) || sumKNumbers(arr, n, sum-arr[n-1], count-1)
    }
}
public func sumKNumbers_dp(_ arr: [Int], _ n: Int, _ sum:Int, _ count:Int)->Int{
    ///base case
    if sum == 0 {
        return count == 0 ? 1 : 0
    }
    if n == 0 {return 0}
    
    var t: [[[Int]]] = Array(repeating: Array(repeating: Array(repeating: 0, count: count+1), count: sum+1), count: n+1)
    print(t[0][0][0])
    for i in 0...n{
        for j in 0...sum{
            for c in 0...count{
                if c == 0 {t[i][j][c] = 1}
                else if i == 0 || j == 0 {t[i][j][c] = 0}
                else{
                    if arr[i-1] > j{
                        t[i][j][c] = t[i-1][j][c]
                    }else{
                        t[i][j][c] = (t[i-1][j][c] == 1 || t[i][j-arr[i-1]][c-1] == 1) ? 1 : 0
                    }
                }
            }
        }
    }
    print(t)
    var i = n, j = sum, k = count, ans:[Int] = []
    
    while  i > 0 && j > 0 && k > 0 {
        print(i, j, k)

        if t[i][j][k] == 1 {
            ans.append(arr[i-1])
            k -= 1
            j -= arr[i-1]
        }else{
            i -= 1
        }
    }
    print(ans)
    return t[n][sum][count]
}


/// rod cutting
/// given rod of length: N
/// profite for lengths: array:[Int]
/// o/p: maximize profit

public func rodCutting(_ pt:[Int], _ N:Int)->Int{
    let wt = Array(1...N)
    let W = N
    let n = pt.count
    
    return unboundedKnapsack(wt, pt, n, W)
}

/// coin change problem
///1. maximum number of ways
/// input : array of coins with unlimited supply, a given total value
/// output: total number of ways to get the total by selecting the coins
public func  coinChangeProblem(_ coins:[Int], sum: Int)->Int{
    return unboundedKnapsackforCoins_dp(coins, coins.count, sum)
}
private func unboundedKnapsackforCoins(_ coins:[Int], _ n:Int, _ sum:Int)->Int {
   /// basecase
    if sum == 0 {return 1}
    if n == 0 {return 0}
    
    /// solve
    if coins[n-1] > sum {
        return unboundedKnapsackforCoins(coins, n-1, sum)
    }else{
        return unboundedKnapsackforCoins(coins, n, sum-coins[n-1]) + unboundedKnapsackforCoins(coins, n-1, sum)
    }
}

private func unboundedKnapsackforCoins_dp(_ coins:[Int], _ n:Int, _ sum:Int)->Int {
    /// basecase
     if sum == 0 {return 1}
     if n == 0 {return 0}
    var t = Array(repeating: Array(repeating: 0, count: sum+1), count: n+1)
    for i in 0...n {t[i][0] = 1}
    for i in 1...n{
        for j in 1...sum{
            if coins[i-1] > j {
                t[i][j] = t[i-1][j]
            }else{
                t[i][j] = t[i][j-coins[i-1]]+t[i-1][j]
                
            }
        }
    }
    return t[n][sum]
}

///2. minimum number of coins
public func minimumNumberOfCoins(_ coins:[Int], _ n: Int, _ sum: Int)->Int{
    /// basecase
    if sum == 0 {return 0}
    if n == 0 {return Int.max-1} /// Int.max-1 to prevent Int overflow when adding 1.
    
    /// solve
    if coins[n-1] > sum {
        return minimumNumberOfCoins(coins, n-1, sum)
    }else{
        return min(minimumNumberOfCoins(coins, n, sum-coins[n-1])+1, minimumNumberOfCoins(coins, n-1, sum))
    }
}

public func minimumNumberOfCoins_dp(_ coins:[Int], _ n: Int, _ sum: Int)->Int{
    if sum == 0 {return 0}
    if n == 0 {return Int.max}
    var t = Array(repeating: Array(repeating: Int.max-1, count: sum+1), count: n+1)
    for i in 0...n{t[i][0] = 0}
    for i in 1...n{
        for j in 1...sum {
            if coins[i-1] > j {
                t[i][j] = t[i-1][j]
            }else{
                t[i][j] = min(1+t[i][j-coins[i-1]], t[i-1][j]) //1+t[i][j-coins[i-1]] may cause Int overflow if [i][j-coins[i-1]] == Int.max
            }
        }
    }
    return t[n][sum]
}
