import Foundation

public func mcm(arr:[Int])->Int{
    var t:[String: Int] = [:]
    return mcmUtil(arr: arr, i: 1, j: arr.count-1, t: &t)
}

private func mcmUtil(arr:[Int], i: Int, j: Int, t: inout [String: Int])->Int{
    /// basecase
    if i >= j {
        t["\(i),\(j)"] = 0
        return 0
    }
    if let ans = t["\(i),\(j)"] {return ans}
    var mn = Int.max
    for k in i..<j{
        let cost = mcmUtil(arr: arr, i: i, j: k, t: &t) + mcmUtil(arr: arr, i: k+1, j: j, t: &t)+arr[i-1]*arr[k]*arr[j]
        mn = min(mn, cost)
    }
    t["\(i),\(j)"] = mn
    return mn
}
