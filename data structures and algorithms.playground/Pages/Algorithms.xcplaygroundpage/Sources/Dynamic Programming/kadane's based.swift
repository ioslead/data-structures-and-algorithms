import Foundation

/// Leetcode:3
public   func lengthOfLongestSubstringWithNonRepeatingCharacters(_ s: String) -> Int {
    let arr = Array(s)
    var ans = 0, dict:[Character:Int] = [:], temp = 0, i = 0
    for j in 0..<arr.count{
        if let index = dict[arr[j]]{
            while i <= index {
                dict[arr[i]] = nil
                i += 1
            }
            temp = j-i+1
        }else {
            temp += 1
            ans = max(ans, temp)
        }
        dict[arr[j]] = j
        
    }
    return ans
}


