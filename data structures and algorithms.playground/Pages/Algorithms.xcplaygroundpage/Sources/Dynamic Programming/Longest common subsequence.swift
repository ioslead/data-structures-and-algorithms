import Foundation

/// Longest common subsequence
public func lcs(_ str1: String, _ str2: String)->Int{
    return lcs_dp(Array(str1), Array(str2), str1.count, str2.count)
//    return lcs_rec(Array(str1), Array(str2), str1.count, str2.count)
}
private func lcs_rec<T:Equatable>(_ arr1: [T], _ arr2: [T], _ m:Int, _ n:Int)->Int{
    /// basecase
    if n == 0 || m == 0 {return 0}
    /// solve
    if arr1[m-1] == arr2[n-1]{
        return 1 + lcs_rec(arr1, arr2, m-1, n-1)
    }else {
        return max(lcs_rec(arr1, arr2, m, n-1), lcs_rec(arr1, arr2, m-1, n))
    }
}

private func lcs_dp<T:Equatable>(_ arr1: [T], _ arr2: [T], _ r:Int, _ c:Int)->Int{
    /// basecase
    if r == 0 || c == 0 {return 0}
    var t = Array(repeating: Array(repeating: 0, count: c+1), count: r+1)
    for i in 0...r {
        for j in 0...c {
            if i==0 || j==0 {t[i][j] = 0}
            else if arr1[i-1] == arr2[j-1]{
                t[i][j] = 1+t[i-1][j-1]
            }else{
                t[i][j] = max(t[i-1][j], t[i][j-1])
            }
        }
    }
    printLCS(t, arr1, arr2)
    return t[r][c]
}

public func printLCS<T:Equatable>(_ t:[[Int]], _ arr1:[T], _ arr2:[T]){
    var r = t.count-1
    var c = t.first?.count ?? 0 ; c = c-1
    
    var ans:[T] = []
    while r > 0 && c > 0 {
        if arr1[r-1] == arr2[c-1] {
            ans.append(arr2[c-1])
            r -= 1; c -= 1;
        }else if t[r-1][c] > t[r][c-1]{
            r -= 1
        }else{
            c -= 1
        }
    }
    ans = ans.reversed()
    print(ans)
}

///
/// Longest common substring : LCSS
/// The code will be ditto same as LCS except when arr1[m-1] !=  arr2[n-1] we will return 0 (recursive) or t[i][j] = 0 for arr1[i-1] != arr2[j-1]
///

///
/// Shortest common supersequence :
/// Given sequence S1 and S2. Find a sequence of min length wich contains both S1 and S2.
/// o/p: length of supersequence
/// solution : S1.count+S2.count - LCS(S1, S2)
///

/// Print Sortest common supersequence:
///
public func printSCS<T:Equatable>(_ t:[[Int]], _ arr1:[T], _ arr2:[T]){
    var r = t.count-1
    var c = t.first?.count ?? 0 ; c = c-1
    
    var ans:[T] = []
    while r > 0 && c > 0 {
        if arr1[r-1] == arr2[c-1] {
            ans.append(arr2[c-1])
            r -= 1; c -= 1;
        }else if t[r-1][c] > t[r][c-1]{
            ans.append(arr1[r-1])
            r -= 1
        }else{
            ans.append(arr2[c-1])
            c -= 1
        }
    }
    ///append remaining
    while r > 0 {
        ans.append(arr1[r-1])
        r -= 1
    }
    while c > 0 {
        ans.append(arr2[c-1])
        c -= 1
    }
    ans = ans.reversed()
    print(ans)
}


/// minimum number of insert and delete operation to convert string S1 to S2.
/// solution:
/// # of deletion,  d = S1 - LCS(S1, S2)
/// # of insertion, i = S2 - LCS(S1, S2)
/// total =  d+i
///

///
/// Longest Palindromic Subsequence : LPS
/// Solution:
/// LPS(str) = LCS(str, str.reversed())
///

///
/// Minimum number of deletion in a string to make it palindrome
/// Solution: str.count - LPS(str)
///

///
/// Minimum number of insertion to make it palindrome
/// Solution: str.count - LPS
///


/// longest repeating subsequence
public func lrs_dp(str:String)->Int{
    let arr = Array(str)
    let arr1 = arr
    let arr2 = arr
    let r = str.count
    let c = str.count
    
    /// basecase
    if r == 0 || c == 0 {return 0}
    var t = Array(repeating: Array(repeating: 0, count: c+1), count: r+1)
    for i in 0...r {
        for j in 0...c {
            if i==0 || j==0 {t[i][j] = 0}
            else if arr1[i-1] == arr2[j-1] && i != j{ /// notice the difference b/w lcs here : && i != j
                t[i][j] = 1+t[i-1][j-1]
            }else{
                t[i][j] = max(t[i-1][j], t[i][j-1])
            }
        }
    }
    return t[r][c]
}

/// sequence pattern matching
/// given : String A and B. Check if B is present (as a subsequence) in A.
/// Solution : LCS(A, B) = B.count
/// a more optimized solution is below: T = O(n+m)
public func spm<T:Equatable>(A:[T], B:[T])->Bool{
    let n = A.count
    var j = 0
    for i in 0..<n {
        if(A[i] == B[j]) {j += 1}
    }
    return j == B.count
}

