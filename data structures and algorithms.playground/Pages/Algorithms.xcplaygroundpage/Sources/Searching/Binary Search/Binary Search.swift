import Foundation


public extension RandomAccessCollection where Element: Comparable {
    func binarySearch(for value: Element, in range: Range<Index>? = nil)->Index?{
        let range = range ?? startIndex..<endIndex
        guard range.lowerBound < range.upperBound else {
            return nil
        }
        let n = distance(from: range.lowerBound, to: range.upperBound)
        let middle = index(range.lowerBound, offsetBy: n/2)
 
        if self[middle] == value {
            return middle
        }else if self[middle] > value{
            return binarySearch(for: value, in: range.lowerBound..<middle)
        }else {
            return binarySearch(for: value, in: index(after: middle)..<range.upperBound)
        }
    }
}

public extension Array where Element : Comparable{
    func binarySearchArr(for value: Element)->Int? {
        var left = 0
        var right = self.count - 1
        while left <= right {
            let middle = (left+right)/2
            if self[middle] == value {return middle}
            if  value > self[middle]{
                left = middle+1
            }else{
                right = middle-1
            }
        }
        return nil
    }
}



//MARK:- Test
public extension Array where Element == Int{
    static func test(){
        
        let array = [1, 5, 15, 17, 19, 22, 24, 31, 105, 150]
        let search31 = array.firstIndex(of: 31) ///O(n)
        let binarySearch31 = array.binarySearch(for: 31) ///O(logn)
        let binarySearch31_arr = array.binarySearchArr(for: 31) ///O(logn)
        print(search31 ?? "")
        print(binarySearch31 ?? "")
        print(binarySearch31_arr ?? "")
    }
}


public extension Array where Element == Int{
    func firstAndLastOccurenceOfElement(_ element: Int)->(Int?, Int?){
        let first = self.firstOccurenceOfElement(element)
        let last = self.lastOccurenceOfElement(element)
        return (first, last)
    }
    private func firstOccurenceOfElement(_ element: Int)->Int?{
        if self.count == 0 {return nil}
        if self.count == 1 {return self[0] == element ? 0 : nil}
        var s = 0, e = count-1, ans:Int?
        while s <= e {
            let mid = s+((e-s)/2) /// equivalent to s+e/2 , this will make sure no Integer overflow happen
            if self[mid] == element {
                ans = mid
                e = mid - 1
            }else if self[mid] < element {
                s = mid+1
            }else{
                e = mid-1
            }
        }
        return ans
    }
    
    private func lastOccurenceOfElement(_ element: Int)->Int?{
        if self.count == 0 {return nil}
        if self.count == 1 {return self[0] == element ? 0 : nil}
        var s = 0, e = count-1, ans:Int?
        while s <= e {
            let mid = s+((e-s)/2) /// equivalent to s+e/2 , this will make sure no Integer overflow happen
            if self[mid] == element {
                ans = mid
                s = mid + 1
            }else if self[mid] < element {
                s = mid+1
            }else{
                e = mid-1
            }
        }
        return ans
    }
}

public extension Array where Element == Int{
    func countNumberOfOccurences(element: Int)->Int{
        let firstLast = firstAndLastOccurenceOfElement(element)
        if let a = firstLast.0, let b = firstLast.1 {
            return b-a+1
        }else{
            return 0
        }
    }
}

public extension Array where Element == Int{
    func numberOfTimesSortedArrayIsRotated()->Int{
        var ans = 0
        if self.count < 2 {return ans}
        var s = 0, e = count-1
        while s <= e {
            let mid = s+((e-s)/2)
            let left = mid > s ? mid-1 : mid
            let right = mid < e ? mid+1 : mid
            if self[mid] <= self[left] && self[mid] <= self[right] {
                ans = count-mid
                break
            }else if self[mid] >= self[s]{
                s = mid+1
            }else if self[mid] <= self[e]{
                e = mid-1
            }
        }
        return ans
    }
}


public extension Array where Element == Int{
    func findAnElementInSortedAndRotated(_ element: Element)->Int?{
         let rotationCount = self.numberOfTimesSortedArrayIsRotated()
        if rotationCount == 0 {
            return self.binarySearchArr(for: element)
        }else{
            let rotationIndex = count - rotationCount
            if let left =  self[0..<rotationIndex].binarySearch(for: element) {return left}
             return self[rotationIndex..<count].binarySearch(for: element)
        }
    }
}

public extension Array where Element == Int{
    /// element at i can be at i-1, i or i+1
    func searchInNearlySorted(_ element: Element)->Int?{
        if count == 0 {return nil}
        var s = 0, e = count-1
        while s<=e {
            let mid = s + (e-s)/2
            if self[mid] == element {return mid}
            if mid > s && self[mid-1] == element {return mid-1}
            if mid < e && self[mid+1] == element {return mid+1}
            if self[mid] > element {
                e = mid-2
            }else{
                s = mid+2
            }
        }
        return nil
    }
}

public extension Array where Element == Int{
    func findFloorOfElement(_ element: Element)->Element?{
        if count == 0 {return nil}
        if count == 1 {return self[0] <= element ? self[0] : nil}
        var s = 0, e = count-1, ans:Int? = nil
        while s <= e {
            let mid = s+((e-s)/2)
            if self[mid]==element{
                ans = element
                break
            }else if self[mid] < element {
                ans = self[mid]
                s = mid+1
            }else{
                e = mid-1
            }
        }
        return ans
    }
    //TODO:- finc ceil of an element
}
public extension String {
    func nextAlphabeticalElement(char:Character)->Character?{
        if count == 0 {return nil}
        var s = 0, e = count-1, ans:Character?
        while s <= e {
            let mid = s + ((e-s)/2)
            let midChar = self[self.index(self.startIndex, offsetBy: mid)]
            if midChar == char {
                if mid < count-1 {return self[self.index(self.startIndex, offsetBy: mid+1)]}
                return nil
            }else if char < midChar {
                ans = midChar
                e = mid-1
            }else{
                s = mid+1
            }
        }
        return ans
    }
}

public extension Array where Element == Int{
    func minimumDifferenceWithElement(_ key: Element)->Element?{
        /// in binary search if key is present we get in index but if not, the 's' and 'e' variables which we use to iterate the search will be pointing to the elements which are closest to the key.
        /// so if self[mid] == key {return 0}
        ///  if e == count {e=count-1}; if s<0 {s=0}
        /// else return min(abs(self[s]-key), abs(key-self[e]))
        return nil
    }
}

/// number of equal pairs in sorted arrays
/// e.g. [1,1,2,2] [1,2,2,3] ==> [(1,1),(1,1),(2,2),(2,2),(2,2),(2,2)] i.e. 6

//1. ans = 0. Take 2 indices i and j for arr1, arr2 respectively and initialise to 0.
//2. increase i or j which one is pointing to the samller element until you get an equal element
//3. get the first and last occurences of the element in both the arrays (easy to write the method).
//4. calculate and count(frequency) : last - first + 1 and update the final answer : ans += count1*count2
//5. now i and j will jump to next (not equal) element : i = last1 + 1, j = last2 + 1
//6. return ans

public func numberOfEqualPairs(_ arr1: [Int], _ arr2: [Int]) ->Int{
    var ans = 0
    let n1 = arr1.count, n2 = arr2.count
    var i = 0, j = 0
    while i < n1 && j < n2 {
        if arr1[i] < arr2[j] {
            i += 1
        }else if arr1[i] > arr2[j] {
            j += 1
        }else {
            let pos1 = arr1.firstAndLastOccurenceOfElement(arr1[i]) //optimization
            let pos2 = arr2.firstAndLastOccurenceOfElement(arr2[j]) //optimization
            let count1 = (pos1.1! - pos1.0! + 1)
            let count2 = (pos2.1! - pos2.0! + 1)
            let numOfPairs = count1*count2
            ans += numOfPairs
            i = pos1.1!+1 //optimization
            j = pos2.1!+1 //optimization
        }
    }
    return ans
}
///Time Complexity: O(nlogn)

/// below is O(logn)

public func countEqualPairs(_ arr1: [Int], _ arr2: [Int]) ->Int{
    let n1 = arr1.count, n2 = arr2.count
    if n1 == 0 || n2 == 0 {
        return 0
    }
    var ans = 0, i=0, j=0
    while i<n1 && j<n2 {
        if arr1[i] < arr2[j]{
            i += 1
        }else if arr1[i] > arr2[j]{
            j += 1
        }else{
            let element = arr1[i]
            var count1 = 0, count2 = 0
            while i < n1, arr1[i] == element{
                count1 += 1
                i += 1
            }
            while j < n2, arr2[j] == element {
                count2 += 1
                j += 1
            }
            ans += count1 * count2
        }
    }
    return ans
}
