import Foundation

/// Print first -ve in window
public extension Array where Element == Int {
     func printFirstNegativeInWindow(_ size: Int){
        if size > count {return}
        var i = 0
        for j in (size-1)..<count{
            while self[i] >= 0 && i < j{
                i += 1
            }
            if self[i] < 0 {
                print(self[i])
            }else {
                print(0)
            }
            if j-i == size-1 { i += 1}
        }
    }

}

func countFirstNegativeIn(_ arr: [Int], forWindowSize k: Int)->Int{
    var ans = 0
    if k > arr.count {return ans}
    var i = 0
    for j in k-1..<arr.count{
        while arr[i] >= 0 && i < j{
            i += 1
        }
        if arr[i] < 0 {
            ans += 1 //or print(arr[i])
        }else {
            //no -ve,  print(0)
        }
        if j-i == k-1 { i += 1}
    }
    return ans
}

