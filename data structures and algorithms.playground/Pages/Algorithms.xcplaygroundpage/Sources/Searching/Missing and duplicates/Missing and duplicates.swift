import Foundation

//MARK:- one missing, one duplicate
public extension Array where Element == Int {
    func findMissingAndDuplicate()->(missing:Int?, duplicate:Int?){ /// numbers from 1....n, one is missing means one will be repeated. Find them
        if self.count < 2 {return (nil, nil)}
        var sum_true = 0
        var sum_false = 0
        for i in 1...self.count{
            sum_true = sum_true + i
        }
 
        for e in self {
            sum_false = sum_false + e
        }
        if sum_true == sum_false {return (nil, nil)}
        
        let diff = sum_false - sum_true ///a-b where a is duplicate and b is missing
        ///calculate a*a - b*b
        var sqr_sum_true = 0
        var sqr_sum_false = 0
        
        for i in 1...self.count{
            sqr_sum_true = sqr_sum_true + i*i
        }
        for e in self {
            sqr_sum_false = sqr_sum_false + e*e
        }
        let diff_sqr = sqr_sum_false - sqr_sum_true /// a^2-b^2
        let sum = diff_sqr/diff /// a+b = (a^2 - b^2) /(a - b)
        let duplicate = (diff+sum)/2
        let missing = sum-duplicate
        return (missing:missing, duplicate:duplicate)
    }
}

//TODO:- hashing or dictionary


//MARK:- using swap sort
public extension Array where Element == Int {
    mutating func findOneOrMoreMissingsAndDuplicates()->(missings:[Int], duplicates:[Int]){
        if self.count < 2 {return ([], [])}
        var i = 0
        var missings:[Int] = []
        var duplicates:[Int] = []
        while i<self.count {
            if self[i] != (i+1) && self[self[i]-1] != self[i]  {
                self.swapAt(i, self[i]-1)
            }else{
                i = i+1
            }
        }
        for i in 0...self.count-1 {
            if self[i] != i+1 {
                missings.append(i+1)
                duplicates.append(self[i])
                
            }
        }
        return (missings, duplicates)
    }
}
