/*
 LinkedList.test()
 QueueArray.test()
 TNode.test()
 BNode.test()
 Tri.test()
 Array.test()
 BSTree.test()
 //print(maxProfit([2,4,1,6,4]))
 */
//var heap = Heap<Int>(sortBy: <)
//let arr = [3,2,5,4,1,12,14,34,32]
//arr.forEach{ e in
//    heap.insert(e)
//}
//[1,2,3,5,6,7,8,5].findMissingAndDuplicate()
//1+4+9+25+36+49+64+25
//[2,1].findMissingAndDuplicate()
var arr_mutating = [1,3,2,3, 1,2,5,7]
//arr_mutating.findOneOrMoreMissingsAndDuplicates()
//[2,3,4,5,5,5,5,6,7,8].firstAndLastOccurenceOfElement(5)
//[2,3,4,5,5,5,5,6,7,8].countNumberOfOccurences(element: 5)
//[4,5,6,7,8,2,2,3].numberOfTimesSortedArrayIsRotated()
//[4,5,6,7,8,2,2,3].findAnElementInSortedAndRotated(2)
//[5,10,30,20,40].searchInNearlySorted(15)
//[1,2,3,5,8,11].findFloorOfElement(0)
//"abgrw".nextAlphabeticalElement(char: "p")
arr_mutating.quickSort()
//var str_mutating =  "ABCDE"
//str_mutating.printPermutationWithSpace()
/*
var closure: (()->())?
func assignClosure(_ closure1: @escaping ()->()){
    closure = closure1
}


knapsack_rec([3,4,2,8], [22, 25, 16, 54], 4, 12)
knapsack_dp([3,4,2,8], [22, 25, 16, 54], 4, 12)
equalSumPartition([1,2,2,2,3,5,6,7])
countSubsetSum([2,1, 7, 5,5], sum: 10)
minimumSubsetSumDifference([2,11,7,5,6])
numberOfSubsetWithGivenDifference([1,1,2,3], diff: 1)
unboundedKnapsack([3,4,2,7], [22, 25, 16, 54], 4, 12)
rodCutting([1,2,5,8,11], 8)
coinChangeProblem([1,2, 3], sum: 4)
minimumNumberOfCoins([1,2,3], 3, 4)
minimumNumberOfCoins_dp([1,2,3], 3, 16)
lcs("pankaj", "neeraj")
lrs_dp(str: "AABEBCDD")

[5,-2,-1,7,6,-5,-1,8,3,1].printFirstNegativeInWindow(3)
//lengthOfLongestSubstringWithNonRepeatingCharacters("pwwkew")
 */
//sumKNumbers_dp([1,2,3], 3, 8, 8)
/*
[1,1,0,1,1,1].maxContinuous1s()

[1,2,2,2,2,2,2,2,2,2,2,1,3,2,2,6,4,3,4,6,1].findMejority()

findIntersectArr([1,2,3,2,4],[2,2])
[1,2,3,1].duplicatesExistWithinDistance(3)
[1,0,1,1].duplicatesExistWithinDistance(1)
[1,2,3,1,2,3].duplicatesExistWithinDistance(2)

var dup_arr = [1,1,3,3,4,5,6,7,7,8] //n=10, n' = 6
dup_arr.removeDuplicatesInSortedArr()
/*
i = 0, j = 1
 while (j<n){
    if arr[i] != arr[j]{
        i++
        arr[i] = arr[j]
    }
    j++
 }
 return arr[0...i]
*/

var dup_arr1 = [1,1,1,2,2,3,3]
dup_arr1.removeDuplicatesAtMostTwiceAllowed()
let n = numberOfEqualPairs([1, 2, 2], [1, 1, 1, 2, 2, 2, 3])
let n1 = countEqualPairs([1,1,2,2], [1,2,2,3])
*/

import Foundation
let currDate = Int64(Date().timeIntervalSince1970)*1000

struct Reward: Codable, Hashable {
    static func == (lhs: Reward, rhs: Reward) -> Bool {
        lhs.rewardId == rhs.rewardId
    }
    func hash(into hasher: inout Hasher) {
        rewardId
    }
    let type: String?
    let rewardId: String?
    let state: String?
    let updatedAt: Int64?
    let expiresAt: Int64?
    let benefit: Benefit?
    
}
struct Benefit:Codable{
    let type: String
    let useBy: Int64
}

let r1 = Reward(type: nil, rewardId: nil, state: nil, updatedAt: nil, expiresAt: nil, benefit: nil)
let r2 = Reward(type: nil, rewardId: nil, state: nil, updatedAt: nil, expiresAt: nil, benefit: nil)

var rewards = [r1, r2]
var setOfRewards = Set(rewards)
 var unscretched = setOfRewards.filter { (card) -> Bool in
    card.state == "UNSCRATCHED" && card.expiresAt! > currDate
}

unscretched.sorted { (r1, r2) -> Bool in
    r1.expiresAt! < r2.expiresAt!
}
setOfRewards = setOfRewards.intersection(unscretched)

//prepare cellModel for above scratched cards

var offerAndCoupons = setOfRewards.filter { (card) -> Bool in
    card.state == "OPENED" && card.benefit!.useBy > currDate && (card.benefit!.type == "OFFOR" || card.benefit!.type == "COUPON")
}

offerAndCoupons.sorted { (r1, r2) -> Bool in
    r1.benefit!.useBy < r2.benefit!.useBy
}

setOfRewards = setOfRewards.intersection(offerAndCoupons)
//prepare cellModel for above offerAndCoupons cards

var allCashback = setOfRewards.filter { (card) -> Bool in
    card.state == "OPENED" && card.benefit!.type == "CASHBACK"
}
allCashback.sorted { (r1, r2) -> Bool in
    r1.updatedAt! > r2.updatedAt!
}
setOfRewards = setOfRewards.intersection(allCashback)

//prepare cellModel for above allCashback cards

//prepare cellModel for remaining setOfRewards cards

//
//
//recoverLossTest()
Int(recoverLoss(numberOfMonths:360, investmentPerMonth: 1000, profitRatePerMonth: 0.05))
